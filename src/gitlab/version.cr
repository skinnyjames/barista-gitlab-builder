require "crest"
require "uri"
require "json"

module Gitlab
  # interface for Gitlab::Version
  #
  # helps with test injection
  module VersionFinder; end

  class RetryExceeded < Exception; end

  # Error raised when failing to fetch
  #
  # Used in `Gitlab::Version`
  class FetchError < Exception; end

  # Fetches and stores versions around GitLab project dependencies
  # 
  # Takes a `version_to_resolve` which can be a [GitLab](http://gitlab.com/gitlab-org/gitlab) branch
  # tag, or commit.
  class Version
    include VersionFinder

    getter(
      :retry,
      :ee,
      :commit_sha,
      :semver_version,
      :gitlab_host,
      :gitlab_project,
      :gitlab_version,
      :gitaly_version,
      :gitlab_internal_version,
      :gitlab_pages_version,
      :gitlab_shell_version,
      :gitlab_workhorse_version,
      :gitlab_metrics_exporter_version,
      :gitlab_kas_version,
      :gitlab_elasticsearch_indexer_version
    )

    @gitlab_host : String
    @gitlab_project : String
    @commit_sha : String
    @gitlab_internal_version : String
    @semver_version : String
    @gitlab_version : String
    @gitaly_version : String
    @gitlab_pages_version : String
    @gitlab_shell_version : String
    @gitlab_workhorse_version : String
    @gitlab_metrics_exporter_version : String
    @gitlab_kas_version : String
    @gitlab_elasticsearch_indexer_version : String

    def initialize(
      version_to_resolve : String = ENV.fetch("GITLAB_VERSION", "master"),
      *,
      @retry : Int32 = 3,
      @ee : Bool = false
    )
      @gitlab_host = ENV.fetch("GITLAB_HOST", "gitlab.com")
      @gitlab_project = ENV.fetch("GITLAB_PROJECT", resolve_gitlab_project)
      @commit_sha = resolve_gitlab_commit(version_to_resolve)
      @gitlab_internal_version = fetch("VERSION", true, default: commit_sha)
      @gitlab_version = fetch_gitlab_version
      @semver_version = semver_version
      @gitaly_version = fetch("GITALY_SERVER_VERSION", true)
      @gitlab_pages_version = fetch("GITLAB_PAGES_VERSION", true)
      @gitlab_shell_version = fetch("GITLAB_SHELL_VERSION", true)
      @gitlab_workhorse_version = fetch("GITLAB_WORKHORSE_VERSION", true)
      @gitlab_metrics_exporter_version = fetch("GITLAB_METRICS_EXPORTER_VERSION", true)
      @gitlab_kas_version = fetch("GITLAB_KAS_VERSION", true)
      @gitlab_elasticsearch_indexer_version = fetch("GITLAB_ELASTICSEARCH_INDEXER_VERSION", true)
    end

    # Is this version Enterprise Edition?
    def ee?
      @ee
    end

    # resolves the true GitLab version for a given `gitlab/VERSION` file.
    #
    # checks and returns the following in this order:
    # * the `VERSION` file contents if it has a tag
    # * the commit sha if `VERSION` is untagged
    def fetch_gitlab_version
      tag_exists?(gitlab_internal_version) ? gitlab_internal_version : commit_sha
    end

    # The content of `gitlab/VERSION` + the commit sha
    def semver_version
      [gitlab_internal_version.gsub(/^v/, ""), commit_sha].join(".")
    end

    # Checks to see if `<tag>` exists on a GitLab project.
    def tag_exists?(tag : String) : Bool
      url = "#{gitlab_api_url}/repository/tags/#{encode_part(tag)}"
      begin
        json_string = Crest.get(url).body
        !json_string.includes?("404")
      rescue Crest::NotFound
        false
      end
    end

    def to_h : Hash(String, String)
      {
        "GITLAB_COMMIT_SHA" => commit_sha,
        "SEMVER_VERSION" => semver_version,
        "GITLAB_VERSION" => gitlab_version,
        "VERSION" => gitlab_internal_version,
        "GITALY_SERVER_VERSION" => gitaly_version,
        "GITLAB_PAGES_VERSION" => gitlab_pages_version,
        "GITLAB_SHELL_VERSION" => gitlab_shell_version,
        "GITLAB_WORKHORSE_VERSION" => gitlab_workhorse_version,
        "GITLAB_METRICS_EXPORTER_VERSION" => gitlab_metrics_exporter_version,
        "GITLAB_KAS_VERSION" => gitlab_kas_version,
        "GITLAB_ELASTICSEARCH_INDEXER_VERSION" => gitlab_elasticsearch_indexer_version
      }
    end

    # Fetches a file from a GitLab source.  Used for fetching `VERSION` files.
    def fetch(file : String, prepend_version : Bool = false, default : String = gitlab_version.gsub(/^v/, "")) : String
      version = fetch_raw_file(file)

      return default if version == "VERSION"
      return version unless /^\d+\.\d+\.\d+(-rc\d+)?(-ee)?(-pre)?$/.matches?(version)

      v = prepend_version ? "v" : ""
      
      [v, version].join
    end

    private def fetch_raw_file(file : String)
      content = ""

      with_retry do
        content = Crest.get("#{gitlab_project_url}/-/raw/#{commit_sha}/#{file}").body.chomp("\n")
      end

      raise FetchError.new("Content for #{file} is empty") unless content

      content
    end

    private def resolve_gitlab_commit(version_to_resolve : String) : String
      commit = fetch_gitlab_commit_from_branch(version_to_resolve) || fetch_gitlab_commit_from_tag(version_to_resolve)
      commit = version_to_resolve if commit.nil? && is_git_rev_hash?(version_to_resolve)

      raise FetchError.new("Couldn't resolve commit for: #{version_to_resolve}") unless commit

      commit
    end

    private def fetch_gitlab_commit_from_branch(branch : String) : String?
      commit = nil

      with_retry do
        json_string = Crest.get("#{gitlab_api_url}/repository/branches/#{encode_part(branch)}").body
        structure = JSON.parse(json_string)

        next(nil) if structure["message"]? == "404 Branch Not Found"
        commit = structure["commit"].as_h["id"].as_s
      rescue ex : Crest::NotFound | JSON::ParseException
        next(nil)
      end      

      commit
    end

    private def fetch_gitlab_commit_from_tag(tag : String) : String?
      tag = "v#{tag}" unless tag.starts_with?("v")
      commit = nil

      with_retry do 
        json_string = Crest.get("#{gitlab_api_url}/repository/tags/#{encode_part(tag)}").body

        structure = JSON.parse(json_string)
        next(nil) if structure["message"]? == "404 Branch Not Found"
        commit = structure["commit"].as_h["id"].as_s
      rescue Crest::NotFound
        next(nil)
      end

      commit
    end

    private def with_retry(current : Int32 = 0, &block : ->)
      begin
        block.call
      rescue ex
        raise RetryExceeded.new("Failed to download after #{@retry} retries: #{ex}") if current >= @retry
        with_retry(current.succ, &block)
      end
    end

    private def resolve_gitlab_project
      ee? ? "gitlab-org/gitlab" : "gitlab-org/gitlab-foss"
    end

    private def gitlab_project_url
      "https://#{gitlab_host}/#{gitlab_project}"
    end

    private def gitlab_api_url
      "https://#{gitlab_host}/api/v4/projects/#{encode_part(gitlab_project)}"
    end

    private def encode_part(str : String)
      URI.encode_path_segment(str)
    end

    private def is_git_rev_hash?(str : String)
      /^[0-9a-f]{7,40}$/i.matches?(str)
    end
  end
end
