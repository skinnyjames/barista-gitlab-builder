module Gitlab
  # A test project mounted at a local path
  class GitlabTest < Barista::Project
    include Gitlab::Common
    file("apache_license", "#{__DIR__}/../../../LICENSE")

    @@name = "gitlab-test"

    def initialize
      super
      
      description("GitLab Testing Edition")
      install_dir("./test/opt/gitlab")
      barista_dir("./test/opt/barista")
    end

    def build(version : String, workers : Int32, filter : Array(String)?)
      clean! if Dir.exists?(install_dir)

      super(version: version, workers: workers)
    end
  end
end