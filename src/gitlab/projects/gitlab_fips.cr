module Gitlab
  # Project for a FIPS build
  # 
  # This is not officially supported.
  class GitlabFIPS < Barista::Project
    include Gitlab::Common
    file("apache_license", "#{__DIR__}/../../../LICENSE")

    @@name = "gitlab-fips"

    def initialize
      super
      
      description("GitLab Enterprise Edition (including NGINX, Postgres, Redis) with FIPS compliance")
      extra_config
    end

    def extra_config
      if rhel?
        runtime_dependency("openssl-perl")
      else
        runtime_dependency("openssl")
      end
    end
  end
end