module Gitlab
  class GitlabCE < Barista::Project
    include Gitlab::Common
    file("apache_license", "#{__DIR__}/../../../LICENSE")

    @@name = "gitlab-ce"

    def initialize
      super

      description("GitLab Community Edition (including NGINX, Postgres, Redis)")
    end
  end
end