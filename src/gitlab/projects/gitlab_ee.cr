module Gitlab
  class GitlabEE < Barista::Project
    include Gitlab::Common
    file("apache_license", "#{__DIR__}/../../../LICENSE")

    @@name = "gitlab-ee"

    def initialize
      super
      
      description("GitLab Enterprise Edition (including NGINX, Postgres, Redis)")
    end

    def build(version : String, workers : Int32, filter : Array(String)?)
      super(version, workers, filter: filter, ee: true)
    end
  end
end