module Gitlab
  # Mixin with methods and state that are included into `Gitlab::Tasks`
  #
  # exposes information around
  # * environment
  # * platform
  module TaskHelpers
    include PlatformHelper
    
    getter :context

    macro included
      def self.use_system_ssl?
        ENV["USE_SYSTEM_SSL"]? == "true"
      end
    end

    def ruby3_build?
      ENV["RUBY3_BUILD"]? == "true"
    end

    # Path to depedency decisions file for [license_finder](https://github.com/pivotal/LicenseFinder)
    def dependency_decisions_path
      File.join(resources_path, "files", "support", "dependency_decisions.yml")
    end

    # Updates a target source folder with the version of config guess 
    # present in Gitlab::Tasks::ConfigGuess
    def update_config_guess(target : String = ".", install : Array(Symbol) = %i[config_guess config_sub])
      unless dependencies.includes?(Gitlab::Tasks::ConfigGuess)
        raise "Please make ConfigGuess a dependency when using this method"
      end

      config_guess_dir = File.join(install_dir, "embedded", "lib", "config_guess")
      dest = File.join(source_dir, target)

      mkdir(dest, parents: true)

      copy("#{config_guess_dir}/config.guess", dest) if install.includes?(:config_guess)
      copy("#{config_guess_dir}/config.sub", dest) if install.includes?(:config_sub)
    end

    def in_pipeline?
      !ENV["CI"]?.nil?
    end

    def resources_path
      project.resources_path
    end

    def use_system_ssl?
      self.class.use_system_ssl?
    end

    def gcc_target
      raise "Not Implemented"
    end

    def no_sourcemaps?
      ENV["NO_SOURCEMAPS"]? == "true" 
    end

    # NOTE: This is the task constructor used in every `Gitlab::Task`
    def initialize(
      @project : Barista::Behaviors::Omnibus::Project,
      *,
      @callbacks : Barista::Behaviors::Omnibus::CacheCallbacks = Barista::Behaviors::Omnibus::CacheCallbacks.new, 
      @context : TaskData = TaskData.new
    )
      super(project, callbacks)
    end

    # TODO: Deprecate
    def with_cache_dest(hash : Hash(String, String)? = {} of String => String) : Hash(String, String)
      with_destdir(hash)
    end

    # depends on Ruby being installed.
    def ruby_version(env : Hash(String, String)) : String?
      lines = [] of String
      bin("ruby", "-e 'puts RUBY_VERSION.match(/\\d+\\.\\d+/)[0]'", env: env)
        .collect_output(lines)
        .execute

      lines.last?.try(&.chomp)
    end

    def is_auto_deploy_tag?
      ENV["CI_COMMIT_REF_NAME"]?.try(&.includes?("-auto-deploy-")) || false
    end
  end
end