require "./platform_helper"
require "./prometheus_flags"
require "barista"

module Gitlab
  # Common Barista::Project functionality provided to all Gitlab projects
  module Common
    include Barista::Behaviors::Omnibus::Project
    include Barista::Behaviors::Software::Emittable
    include PlatformHelper

    def root
      __DIR__
    end

    def initialize
      install_dir("/opt/gitlab")
      barista_dir("/opt/barista")
      license("Apache-2.0")
      license_content(file("apache_license"))
      replace("gitlab")
      conflict("gitlab")
      package_user("root")
      package_group("root")
      maintainer("GitLab Quality <quality@gitlab.com>")
      homepage("https://gitlab.com/gitlab-org/quality/barista-gitlab-builder")
      
      if ENV["USE_CACHE"]? == "true" || !ENV["CI"]?
        cache(true)

        prefix = [
          "gitlab",
          os_platform,
          kernel.machine.gsub(/\s/, "_")
        ].join("-")

        cache_tag_prefix(prefix)
      end

      configure_runtime_dependencies
      configure_excludes

      package_scripts_path(File.join(install_dir, ".package_util", "package-scripts"))
    end

    def build(version : String, workers : Int32, filter : Array(String)? = nil, ee : Bool = false, finder : Gitlab::VersionFinder = Gitlab::Version.new(version, ee: ee))
      FileUtils.mkdir_p(log_directory)

      build_version(finder.semver_version)

      taskdata = TaskData.new(
        version: finder,
        prometheus_flags: PrometheusFlags.new
      )

      colors = Barista::ColorIterator.new

      Log.setup_from_env

      tasks.each do |task_klass|
        logger = Barista::RichLogger.new(colors.next, task_klass.name)

        task = task_klass.new(self, callbacks: callbacks, context: taskdata)

        task.on_output do |str|
          if task.name == "package-and-push"
            logger.info { str }
          else
            logger.debug { str }
          end

          file_map[task.name].try(&.puts(str))
        end

        task.on_error do |str|
          logger.error { str }

          file_map[task.name].try(&.puts(str))
        end
      end

      orchestration = orchestrator(workers: workers, filter: filter)
         
      orchestration.on_task_start do |task|
        Barista::Log.info(task) { "starting build" }

        file_map[task] = File.new("#{log_directory}/#{task}.log", "w")
      end

      orchestration.on_task_failed do |task, ex|
        Barista::Log.error(task) { "build failed: #{ex}" }
      end

      orchestration.on_task_succeed do |task|
        Barista::Log.info(task) { "build succeeded" }
      end

      orchestration.on_task_finished do |task|
        file_map[task].try(&.close)
        file_map.delete(task)
      end

      orchestration.on_unblocked do |info|
        str = <<-EOH
        Unblocked #{info.unblocked.join(", ")}
        Building #{info.building.join(", ")}
        Active Sequences #{info.active_sequences.map {|k,v| "{ #{k}, #{v} }"}.join(", ")}
        EOH
        Barista::Log.info(name) { str }
      end

      orchestration.execute
    end

    def log_directory
      !ENV["CI"]?.nil? ? "#{ENV["CI_PROJECT_DIR"]}/log" : "#{barista_dir}/log"
    end

    def file_map : Hash(String, File)
      @file_map ||= {} of String => File
    end

    private def configure_runtime_dependencies
      if suse?
        runtime_dependency("openssh")
      else
        runtime_dependency("openssh-server")
      end

      runtime_dependency("perl")

      if rhel?
        case get_centos_version
        when "6", "7"
          runtime_dependency("policycoreutils-python")
        when "8"
          runtime_dependency("policycoreutils-python-utils")
        end
      end

      runtime_dependency("policycoreutils-python") if amazon? && get_amazon_version == "2"

      if arm?
        if rhel? || amazon?
          runtime_dependency("libatomic")
        else
          runtime_dependency("libatomic1")
        end
      end
    end

    private def callbacks : Barista::Behaviors::Omnibus::CacheCallbacks
      callbacks = Barista::Behaviors::Omnibus::CacheCallbacks.new
      callbacks.fetch do |cacher|
        dir = Dir.tempdir
        cache_path = File.join("/cache", "gitlab", cacher.filename)
        begin
          if File.exists?(cache_path)
            FileUtils.cp_r(cache_path, dir)
            cacher.unpack(File.join(dir, cacher.filename))
          else
            false
          end
        rescue ex
          false
        end
      end

      callbacks.update do |task, path|
        FileUtils.mkdir_p("/cache/gitlab") unless Dir.exists?("/cache/gitlab")
        FileUtils.cp(path, File.join("/cache", "gitlab", "#{task.tag}.tar.gz"))
        true
      end

      callbacks
    end

    private def configure_excludes
      exclude("\.git*")
      exclude("bundler\/git")
      
      # don't ship static libraries or header files
      exclude("embedded/lib/**/*.a")
      exclude("embedded/lib/**/*.la")
      exclude("embedded/include")
      
      # exclude manpages and documentation
      exclude("embedded/man")
      exclude("embedded/share/doc")
      exclude("embedded/share/gtk-doc")
      exclude("embedded/share/info")
      exclude("embedded/share/man")
      
      # exclude rubygems build cache
      exclude("embedded/lib/ruby/gems/*/cache")
      
      # exclude test and some vendor folders
      exclude("embedded/lib/ruby/gems/*/gems/*/spec")
      exclude("embedded/lib/ruby/gems/*/gems/*/test")
      exclude("embedded/lib/ruby/gems/*/gems/*/tests")
      # Some vendor folders (e.g. licensee) are needed by GitLab.
      # For now, exclude the most space-consuming gems until
      # there's a better way to whitelist directories.
      exclude("embedded/lib/ruby/gems/*/gems/rugged*/vendor")
      exclude("embedded/lib/ruby/gems/*/gems/ace-rails*/vendor")
      exclude("embedded/lib/ruby/gems/*/gems/libyajl2*/**/vendor")
      
      # exclude gem build logs
      exclude("embedded/lib/ruby/gems/*/extensions/*/*/*/mkmf.log")
      exclude("embedded/lib/ruby/gems/*/extensions/*/*/*/gem_make.out")
      
      # # exclude C sources
      exclude("embedded/lib/ruby/gems/*/gems/*/ext/*.c")
      exclude("embedded/lib/ruby/gems/*/gems/*/ext/*/*.c")
      exclude("embedded/lib/ruby/gems/*/gems/*/ext/*.o")
      exclude("embedded/lib/ruby/gems/*/gems/*/ext/*/*.o")
      
      # # exclude other gem files
      exclude("embedded/lib/ruby/gems/*/gems/*/*.gemspec")
      exclude("embedded/lib/ruby/gems/*/gems/*/*.md")
      exclude("embedded/lib/ruby/gems/*/gems/*/*.rdoc")
      exclude("embedded/lib/ruby/gems/*/gems/*/*.sh")
      exclude("embedded/lib/ruby/gems/*/gems/*/*.txt")
      exclude("embedded/lib/ruby/gems/*/gems/*/*.ruby")
      exclude("embedded/lib/ruby/gems/*/gems/*/*LICENSE*")
      exclude("embedded/lib/ruby/gems/*/gems/*/CHANGES*")
      exclude("embedded/lib/ruby/gems/*/gems/*/Gemfile")
      exclude("embedded/lib/ruby/gems/*/gems/*/Guardfile")
      exclude("embedded/lib/ruby/gems/*/gems/*/README*")
      exclude("embedded/lib/ruby/gems/*/gems/*/Rakefile")
      exclude("embedded/lib/ruby/gems/*/gems/*/run_tests.rb")
      
      exclude("embedded/lib/ruby/gems/*/gems/*/Documentation")
      exclude("embedded/lib/ruby/gems/*/gems/*/bench")
      exclude("embedded/lib/ruby/gems/*/gems/*/contrib")
      exclude("embedded/lib/ruby/gems/*/gems/*/doc")
      exclude("embedded/lib/ruby/gems/*/gems/*/doc-api")
      exclude("embedded/lib/ruby/gems/*/gems/*/examples")
      exclude("embedded/lib/ruby/gems/*/gems/*/fixtures")
      exclude("embedded/lib/ruby/gems/*/gems/*/gemfiles")
      exclude("embedded/lib/ruby/gems/*/gems/*/libtest")
      exclude("embedded/lib/ruby/gems/*/gems/*/man")
      exclude("embedded/lib/ruby/gems/*/gems/*/sample_documents")
      exclude("embedded/lib/ruby/gems/*/gems/*/samples")
      exclude("embedded/lib/ruby/gems/*/gems/*/sample")
      exclude("embedded/lib/ruby/gems/*/gems/*/script")
      exclude("embedded/lib/ruby/gems/*/gems/*/t")
      
      # Exclude additional files from specific gems
      exclude("embedded/lib/ruby/gems/*/gems/grpc-*/include")
      exclude("embedded/lib/ruby/gems/*/gems/grpc-*/src/core")
      exclude("embedded/lib/ruby/gems/*/gems/grpc-*/src/ruby/ext")
      exclude("embedded/lib/ruby/gems/*/gems/grpc-*/src/ruby/spec")
      exclude("embedded/lib/ruby/gems/*/gems/grpc-*/third_party")
      exclude("embedded/lib/ruby/gems/*/gems/nokogumbo-*/ext")
      exclude("embedded/lib/ruby/gems/*/gems/rbtrace-*/ext/src")
      exclude("embedded/lib/ruby/gems/*/gems/rbtrace-*/ext/dst")
      exclude("embedded/lib/ruby/gems/*/gems/*pg_query-*/ext")
      
      # Exclude exe files from Python libraries
      exclude("embedded/lib/python*/**/*.exe")
      # Exclude whl files from Python libraries.
      exclude("embedded/lib/python*/**/*.whl")

      exclude(".package_util")

      # Exclude Python cache and distribution info
      exclude("embedded/lib/python*/**/*.dist-info")
      exclude("embedded/lib/python*/**/*.egg-info")
      exclude("embedded/lib/python*/**/__pycache__")

      # exclude Spamcheck application source and libraries
      exclude("embedded/service/spamcheck/app")
    end
  end
end
