module Gitlab
  # Helper module included in tasks to provide platform information
  module PlatformHelper
    def fetch_os_with_codename
      os = os_platform
      version = os_platform_version
      arch = kernel.machine

      if os == "unknown" || version == "unknown"
        raise "Unsupported OS: #{platform.family} - #{platform.version}"
      end

      { os, version, arch }
    end

    def platform_dir
      os, codename, arch = fetch_os_with_codename

      return "#{os}-#{codename}_#{arch}" if arm64?

      return "#{os}-#{codename}_fips" if use_system_ssl?
    
      "#{os}-#{codename}"
    end

    def os_platform
      case platform.family
      when "ubuntu"
        "ubuntu"
      when "debian", "rasbian"
        platform.family
      when "centos"
        "el"
      when "opensuse", "opensuseleap"
        "opensuse"
      when "suse"
        "sles"
      when "amazon", "aws", "amzn"
        "amazon"
      else
        "unknown"
      end
    end

    def get_ubuntu_version
      case platform.version
      when /^12\.04/
        "precise"
      when /^14\.04/
        "trusty"
      when /^16\.04/
        "xenial"
      when /^18.04/
        "bionic"
      when /^20\.04/
        "focal"
      when /^22\.04/
        "jammy"
      end
    end

    def get_debian_version
      case platform.version
      when /^7/
        "wheezy"
      when /^8/
        "jessie"
      when /^9/
        "stretch"
      when /^10/
        "buster"
      when /^11/
        "bullseye"
      end
    end

    def get_centos_version
      case platform.version
      when /^6\./
        "6"
      when /^7\./
        "7"
      when /^8\./
        "8"
      end
    end

    def get_suse_version
      case platform.version
      when /^15\.2/
        "15.2"
      when /^12\.2/
        "12.2"
      when /^12\.5/
        "12.5"
      when /^11\./
        "11.4"
      end
    end

    def get_amazon_version
      platform.version.try(&.split(".").first)
    end

    def os_platform_version
      case platform.family
      when "ubuntu"
        get_ubuntu_version
      when "debian", "raspbian"
        get_debian_version
      when "centos"
        get_centos_version
      when "opensuse", "opensuseleap"
        platform.version
      when "suse"
        get_suse_version
      when "amazon", "aws", "amzn"
        get_amazon_version
      else
        "unknown"
      end
    end

    def armhf?
      # armv* (Arm 32-bit)
      /armv/.matches?(kernel.machine || "")
    end
    
    def arm64?
      # AArch64 (Arm 64-bit)
      /aarch64/.matches?(kernel.machine || "")
    end
    
    def arm?
      # Any Arm (32-bit or 64-bit)
      (armhf? || arm64?)
    end
    
    def raspberry_pi?
      os_platform == "raspbian"
    end

    def ruby_native_gems_unsupported?
      %w[
        el-8_aarch64
        amazon-2_aarch64
        debian-buster_aarch64
        raspbian-buster_aarch64
      ].includes?(platform_dir)
    end
  end
end
