require "./version"

module Gitlab
  # Enscapsulated data that is injected into the constructors of `Gitlab::Tasks`
  #
  # If the class is a `Gitlab::Task` it has access to these methods.
  struct TaskData
    getter :version, :prometheus_flags

    def initialize(@version : Gitlab::VersionFinder = Gitlab::Version.new, @prometheus_flags : PrometheusFlags = PrometheusFlags.new); end
  end
end