@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Exiftool < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "exiftool"

  file("lib_location_patch", "#{__DIR__}/../../patches/exiftool/lib-location.patch")
  file("license_patch", "#{__DIR__}/../../patches/exiftool/add-license-file.patch")
  file("allowlist_patch", "#{__DIR__}/../../patches/exiftool/allowlist-types.patch")

  def build : Nil
    # exiftool has a hardcoded list of locations where it looks for libraries. We
    # patch it to add the bundled one
    patch(file("lib_location_patch"), string: true)
    # exiftool doesn't provide a license file, so we create one based on info
    # from the website
    patch(file("license_patch"), string: true)
    # Only support JPEG and TIFF files
    patch(file("allowlist_patch"), string: true)

    mkdir(File.join(target, "bin"), parents: true)
    sync("lib", File.join(target, "lib", "exiftool-perl"))
    copy("exiftool", File.join(target, "bin"))
  end

  def target
    File.join(smart_install_dir, "embedded")
  end

  def configure : Nil
    version("12.42")
    license("GPL-1.0 or Artistic")
    license_file("LICENSE.txt")
    source("https://github.com/exiftool/exiftool/archive/refs/tags/#{version}.tar.gz")
  end
end