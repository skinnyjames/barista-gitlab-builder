@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Libedit < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libedit"

  dependency Ncurses
  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    update_config_guess

    command("./configure --prefix=#{install_dir}/embedded", env: env)
    command("make -j 3", env: env)
    command("make -j 3 install", env: env)
  end

  def configure :  Nil
    license("BSD-3-Clause")
    license_file("COPYING")
    version("20120601-3.0")
    source("http://www.thrysoee.dk/editline/libedit-#{version}.tar.gz",
      sha256: "51f0f4b4a97b7ebab26e7b5c2564c47628cdb3042fd8ba8d0605c719d2541918")
  end
end