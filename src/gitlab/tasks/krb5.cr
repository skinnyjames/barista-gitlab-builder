@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Krb5 < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "krb5"

  file("build_patch", "#{__DIR__}/../../patches/krb5/Add-option-to-build-without-libkeyutils.patch")

  dependency OpenSSL unless use_system_ssl?

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    
    # Add configure option allowing libkeyutils to be disabled even if
    # it is detected
    #
    # TEMPORARY: 1.17 uses configure.in, but because autoconf is moving to
    # having this file named configure.ac, the upstream patches that now
    command("mv #{krb_src_dir}/configure.in #{krb_src_dir}/configure.ac")
    patch(file("build_patch"), string: true)
    command("mv #{krb_src_dir}/configure.ac #{krb_src_dir}/configure.in")

    command("autoreconf", env: env, chdir: krb_src_dir)
    command("./configure --prefix=#{install_dir}/embedded --without-system-verto --without-keyutils --disable-pkinit", env: env, chdir: krb_src_dir)
    command("make -j 3", env: env, chdir: krb_src_dir)
    command("make install", env: env, chdir: krb_src_dir)
  end

  def krb_src_dir
    File.join(source_dir, "src")
  end

  def configure :  Nil
    # omnibus shows krb5-1.17
    version("1.17.2")
    source("https://github.com/krb5/krb5/archive/refs/tags/krb5-#{version}-final.tar.gz")
    license("MIT")
    license_file("NOTICE")
    project.exclude("embedded/bin/krb5-config")
  end
end