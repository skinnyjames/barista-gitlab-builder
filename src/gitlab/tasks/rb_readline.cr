@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::RbReadline < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "rb-readline"

  dependency Ruby

  def build : Nil
    env = with_embedded_path

    bin("ruby", "setup.rb", env: env)
  end

  def configure :  Nil
    cache(false)
    license("BSD-3-Clause")
    license_file("LICENSE")
    version("0.5.5")
    source("https://github.com/ConnorAtherton/rb-readline/archive/refs/tags/v#{version}.tar.gz")
  end
end