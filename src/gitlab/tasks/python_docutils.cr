@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::PythonDocutils < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "python-docutils"

  file("license_patch", "#{__DIR__}/../../patches/python-docutils/license/0.16/add-license-file.patch")

  dependency Python3

  def build : Nil
    patch(file("license_patch"), string: true)
    env = with_standard_compiler_flags(with_embedded_path)
    bin("pip3", "install --compile docutils==#{version}", env: env)
  end

  def configure :  Nil
    cache(false)
    license("Public-Domain")
    license_file("COPYING.txt")
    version("0.16")
  end
end