@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::DockerDistributionPruner < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "docker-distribution-pruner"

  def build : Nil
    env = {
      "GOPATH" => File.join(project.source_dir, "docker-distribution-pruner")
    }

    command("go build -ldflags '-s -w' ./cmds/docker-distribution-pruner", env: env)
    mkdir(target, parents: true)
    copy("docker-distribution-pruner", target)
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure : Nil
    version("0.2.0")
    relative_path("src/gitlab.com/gitlab-org/docker-distribution-pruner")
    source("https://gitlab.com/gitlab-org/docker-distribution-pruner/-/archive/v#{version}/docker-distribution-pruner-v#{version}.tar.gz")
    license("MIT")
    license_file("LICENSE")
  end
end
