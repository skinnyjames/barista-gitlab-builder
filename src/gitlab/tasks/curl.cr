@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Curl < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "curl"

  dependency Zlib
  dependency OpenSSL unless use_system_ssl?
  dependency Libtool

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("autoreconf -fi", env: env)
    command(build_config.join(" "), env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure : Nil
    # support vendor
    # vendor("haxx")
    license("MIT")
    license_file("COPYING")
    version("7.86.0")
    source("https://curl.se/download/curl-#{version}.tar.gz")
    project.exclude("embedded/bin/curl-config")
  end

  def build_config
    cmd = [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-option-checking",
      "--disable-manual",
      "--disable-debug",
      "--enable-optimize",
      "--disable-ldap",
      "--disable-ldaps",
      "--disable-rtsp",
      "--enable-proxy",
      "--disable-pop3",
      "--disable-imap",
      "--disable-smtp",
      "--disable-gopher",
      "--disable-dependency-tracking",
      "--enable-ipv6",
      "--without-libidn2",
      "--without-librtmp",
      "--without-zsh-functions-dir",
      "--without-fish-functions-dir",
      "--disable-mqtt",
      "--without-libssh2",
      "--without-nghttp2",
      "--with-zlib=#{install_dir}/embedded",
      "--without-ca-path",
      "--without-ca-bundle",
      "--with-ca-fallback",
    ]
    sslflag = "--with-openssl"
    sslflag += "=#{File.join(install_dir, "embedded")}" unless use_system_ssl?
    cmd << sslflag
    cmd
  end
end