@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Registry < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "registry"

  def build : Nil
    env = {
      "GOPATH" => "#{project.source_dir}/registry",
      "BUILDTAGS" => "include_gcs include_oss"
    }
    
    command("make build", env: env)
    command("make binaries", env: env)

    mkdir(target, parents: true)
    sync("bin", target)

    license_finder
  end

  def license_finder
    cmd = String.build do |io|
      io << "license_finder report "
      io << "--enabled-package-managers godep gomodules "
      io << "--decisions-file=#{dependency_decisions_path} "
      io << " --format=json --columns name version licenses texts notice "
      io << "--save=license.json"
    end
    command(cmd)
    mkdir(File.join(smart_install_dir, "licenses"), parents: true)
    copy("license.json", File.join(smart_install_dir, "licenses", "registry.json"))
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure :  Nil
    license("Apache-2.0")
    license_file("LICENSE")
    version("3.61.0")
    source("https://gitlab.com/gitlab-org/container-registry/-/archive/v#{version}-gitlab/container-registry-v#{version}-gitlab.tar.gz")
    relative_path("src/github.com/docker/distribution")
  end
end