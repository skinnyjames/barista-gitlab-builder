@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::ChefAcme < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  dependency GitlabCookbooks
  dependency AcmeClient
  dependency CompatResource

  @@name = "chef-acme"

  def build : Nil
    sync(".", target_path)
  end

  def target_path
    File.join(smart_install_dir, "embedded", "cookbooks", "acme")
  end

  def configure : Nil
    version("4.1.5")
    license("Apache-2.0")
    license_file("LICENSE")
    source("https://github.com/schubergphilis/chef-acme/archive/refs/tags/v#{version}.tar.gz")
  end
end
