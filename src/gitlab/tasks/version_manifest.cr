@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::VersionManifest < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "version-manifest"

  def build : Nil
    # the package installer relies on semantic whitespace in the manifest.json
    # so we need to pretty print it.
    block do
      File.write("#{install_dir}/version-manifest.txt", manifest_text)
      File.write("#{install_dir}/version-manifest.json", project.manifest.to_pretty_json)
    end
  end

  # omnibus-gitlab adds a formatted text representation of the manifest here
  # but we already have a json document, and it only appears to use the 
  # project name and build version for upgrading.
  def manifest_text
    String.build do |io|
      io.puts "#{project.name} #{project.build_version}"
    end
  end

  def configure :  Nil
    cache(false)
    version("0.0.1")
    license(project.license)
  end
end