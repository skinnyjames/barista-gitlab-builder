@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::PackageScripts < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "package-scripts"

  file("postinst", "#{__DIR__}/../../templates/package-scripts/postinst.hbs")
  file("postrm", "#{__DIR__}/../../templates/package-scripts/postrm.hbs")
  file("posttrans", "#{__DIR__}/../../templates/package-scripts/posttrans.hbs")
  file("preinst", "#{__DIR__}/../../templates/package-scripts/preinst.hbs")
  file("external_url", "#{__DIR__}/../../templates/package-scripts/external_url.sh")

  # we can cache because all the templates are shipped with the binary
  def build : Nil
    mkdir(target, parents: true)
    %w[postinst postrm posttrans preinst].each do |file|
      template(
        src: file(file),
        dest: File.join(target, file),
        mode: File::Permissions.new(0o755),
        vars: {
          "install_dir" => install_dir,
          "external_url_script" => file("external_url"),
          "build_version" => project.build_version, 
          "major" => major,
          "minor" => minor
        },
        string: true
      )
    end
  end

  def major
    project.build_version.split(".")[0]
  end

  def minor
    project.build_version.split(".")[0,2].join(".")
  end

  def target
    File.join(smart_install_dir, ".package_util", "package-scripts")
  end

  def configure :  Nil
    license("Apache-2.0")
    virtual(true)
    version(context.version.gitlab_version)
  end
end