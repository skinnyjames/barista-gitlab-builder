@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Pcre2 < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "pcre2"

  dependency Libedit
  dependency Ncurses
  dependency ConfigGuess
  dependency Libtool

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["CFLAGS"] += " -std=c99"

    update_config_guess

    command("./autogen.sh", env: env)
    command("./configure --prefix=#{install_dir}/embedded --disable-cpp --enable-jit --enable-pcre2test-libedit", env: env)
    command("make -j 2", env: env)
    command("make install", env: env)
  end

  def configure :  Nil
    project.exclude("embedded/bin/pcre2-config")
    license("BSD-2-Clause")
    license_file("LICENCE")
    version("10.40")
    source("https://gitlab.com/gitlab-org/build/omnibus-mirror/pcre2/-/archive/pcre2-#{version}/pcre2-pcre2-#{version}.tar.gz")
  end
end