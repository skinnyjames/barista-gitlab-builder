@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
class Gitlab::Tasks::GitlabPgCtl < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-pg-ctl"

  def build : Nil
    mkdir(target, parents: true)

    block do
      File.open("#{target}/gitlab-pg-ctl", "w") { |file| file.print(script) }
    end

    command("chmod 755 #{target}/gitlab-pg-ctl")
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def script
    str = <<-EOH
    #!/bin/sh
    
    error_echo()
    {
      echo "$1" 2>& 1
    }

    gitlab_psql_rc='/opt/gitlab/etc/gitlab-psql-rc'

    if ! [ -f ${gitlab_psql_rc} ] || ! [ -r ${gitlab_psql_rc} ] ; then
      error_echo "$0 error: could not load ${gitlab_psql_rc}"
      error_echo "Either you are not allowed to read the file, or it does not exist yet."
      error_echo "You can generate it with:   sudo gitlab-ctl reconfigure"
      exit 1
    fi

    . "${gitlab_psql_rc}"

    if [ "$(id -n -u)" = "${psql_user}" ] ; then
      privilege_drop=''
    else
      privilege_drop="-u ${psql_user}:${psql_group}"
    fi

    export PGDATA=${psql_host}/data
    cd /tmp; exec /opt/gitlab/embedded/bin/chpst ${privilege_drop} /opt/gitlab/embedded/bin/pg_ctl "$@"
    EOH
    str
  end

  def configure : Nil
    version(Digest::MD5.new.file(__FILE__).final.hexstring)
    license("Apache-2.0")
  end
end