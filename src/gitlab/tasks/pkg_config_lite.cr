@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::PkgConfigLite < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "pkg-config-lite"

  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    update_config_guess

    command(config_cmd.join(" "), env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def config_cmd
    [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-host-tool",
      "--with-pc-path=#{install_dir}/embedded/bin/pkgconfig"
    ]
  end

  def configure :  Nil
    license("GPL-2.0")
    license_file("COPYING")
    version("0.28-1")
    source("https://downloads.sourceforge.net/project/pkgconfiglite/#{version}/pkg-config-lite-#{version}.tar.gz")
    project.exclude("embedded/bin/pkg-config")
    project.exclude("embedded/lib/pkgconfig")
  end
end