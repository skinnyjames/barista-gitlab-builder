@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::LibtensorflowLite < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libtensorflow-lite"

  def build : Nil
    env = {} of String => String

    mkdir(tf_build_dir, parents: true)
    mkdir(File.join(smart_install_dir, "embedded", "lib"), parents: true)

    block "use a customer compiler for OSs with an older gcc" do
      if platform.family == "centos" && platform.version.try(&.starts_with?("7."))
        env["CC"] = "/opt/rh/devtoolset-8/root/usr/bin/gcc"
        env["CXX"] = "/opt/rh/devtoolset-8/root/usr/bin/g++"
      elsif platform.family == "suse" && platform.version.try(&.starts_with?("12."))
        env["CC"] = "/usr/bin/gcc-5"
        env["CXX"] = "/usr/bin/g++-5"
      elsif platform.family == "opensuse" && platform.version.try(&.starts_with?("15."))
        env["CC"] = "/usr/bin/gcc-8"
        env["CXX"] = "/usr/bin/g++-8"
      elsif platform.family == "amazon" && platform.version.try(&.starts_with?("2"))
        env["CC"] = "/usr/bin/gcc10-gcc"
        env["CXX"] = "/usr/bin/gcc10-g++"
      end
    end

    command("cmake #{source_dir}/tensorflow/lite/c", chdir: tf_build_dir, env: env)
    command("cmake --build . -j 3", chdir: tf_build_dir, env: env)
    copy("#{tf_build_dir}/libtensorflowlite_c.*", "#{smart_install_dir}/embedded/lib")
  end

  def tf_build_dir
    File.join(source_dir, "tflite_build")
  end

  def configure :  Nil
    version("2.6.0")
    license("Apache-2.0")
    license_file("LICENSE")
    source("https://github.com/tensorflow/tensorflow/archive/refs/tags/v#{version}.tar.gz")
  end
end