@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Bundler < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "bundler"

  file("license_patch", "#{__DIR__}/../../patches/bundler/add-license-file.patch")

  dependency Ruby
  sequence ["Gem"]

  def build : Nil
    patch(file("license_patch"), string: true)
    env = with_standard_compiler_flags(with_embedded_path)

    bin("gem", "install bundler --no-document --version '#{version}' --force", env: env)
  end

  def configure : Nil
    version("2.3.24") # omnibus-gitlab uses 2.3.15
    cache(false)
    license("MIT")
    license_file("LICENSE")
  end
end