@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::OmnibusCtl < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "omnibus-ctl"

  dependency Bundler
  sequence ["Gem"]

  file("skip_license_patch", "#{__DIR__}/../../patches/omnibus-ctl/skip-license-acceptance.patch")

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    patch(file("skip_license_patch"), string: true)

    # Remove existing built gems in case they exist in the current dir
    command("rm -f omnibus-ctl-*.gem")

    bin("gem", "build omnibus-ctl.gemspec", env: env)
    bin("gem", "install omnibus-ctl-*.gem --no-document", env: env)

    mkdir("#{install_dir}/embedded/service/omnibus-ctl", parents: true)
    command("touch #{install_dir}/embedded/service/omnibus-ctl/.gitkeep")
  end

  def configure :  Nil
    cache(false)
    version("0.6.0.1")
    license("Apache-2.0")
    license_file("LICENSE")
    source("https://gitlab.com/gitlab-org/build/omnibus-mirror/omnibus-ctl/-/archive/#{version}/omnibus-ctl-#{version}.tar.gz")
  end
end