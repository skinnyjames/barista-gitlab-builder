@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Libxslt < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libxslt"

  dependency Libxml2
  dependency Liblzma
  dependency ConfigGuess

  def build : Nil
    update_config_guess

    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    cmd = [
      "./configure --prefix=#{install_dir}/embedded",
      "--with-libxml-prefix=#{install_dir.sub("C:", "/C")}/embedded",
      "--without-python",
      "--without-crypto"
    ]

    command(cmd.join(" "), env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure :  Nil
    license("MIT")
    license_file("COPYING")
    version("1.1.35")
    source("https://download.gnome.org/sources/libxslt/1.1/libxslt-#{version}.tar.xz",
      sha256: "8247f33e9a872c6ac859aa45018bc4c4d00b97e2feac9eebc10c93ce1f34dd79")

    project.exclude("embedded/lib/xsltConf.sh")
    project.exclude("embedded/bin/xslt-config")
  end
end