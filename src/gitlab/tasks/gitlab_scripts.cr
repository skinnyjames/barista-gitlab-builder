@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabScripts < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-scripts"

  def build : Nil
    mkdir(target, parents: true)
    sync(scripts_path, target)
  end

  def target
    File.join(install_dir, "embedded", "bin")
  end

  def scripts_path
    File.join(resources_path, "files", "gitlab-scripts")
  end

  def configure :  Nil
    license("Apache-2.0")
    cache(false)
    virtual(true)
  end
end