@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabShell < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  dependency Ruby

  @@name = "gitlab-shell"

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    mkdir(target, parents: true)
    command("make build", env: env)
    sync(".", target, exclude: %w[.git .gitignore go go.build])
  end

  def target
    File.join(smart_install_dir, "embedded", "service", "gitlab-shell")
  end

  def configure :  Nil
    version(context.version.gitlab_shell_version)
    license("MIT")
    license_file("LICENSE")
    source("https://gitlab.com/gitlab-org/gitlab-shell/-/archive/#{version}/gitlab-shell-#{version}.tar.gz")
  end
end