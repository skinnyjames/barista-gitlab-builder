@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
class Gitlab::Tasks::Consul < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "consul"

  file("license_patch", "#{__DIR__}/../../patches/config_guess/add-license-file.patch")

  def build : Nil
    env = {
      "GOPATH" => consul_path,
      "PATH" => [ENV["PATH"]?, File.join(consul_path, "bin")].reject(&.nil?).join(":")
    }

    command("make dev", env: env)
    mkdir(target, parents: true)
    copy("bin/consul", target)

    license_finder_command
  end

  def configure : Nil
    version("1.12.5")
    license("MPL-2.0")
    license_file("LICENSE")
    relative_path("src/github.com/hashicorp/consul")
    source("https://github.com/hashicorp/consul/archive/refs/tags/v#{version}.tar.gz")
  end

  def license_finder_command
    cmd = String.build do |io|
      io << "license_finder report "
      io << "--enabled-package-managers godep gomodules "
      io << "--decisions-file=#{dependency_decisions_path} "
      io << "--format=json --columns name version licenses texts notice --save=license.json"
    end

    command(cmd)

    mkdir(File.join(smart_install_dir, "licenses"), parents: true)
    copy("license.json", File.join(smart_install_dir, "licenses", "consul.json"))
  end
  
  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def consul_path
    File.join(project.source_dir, "consul")
  end

  def target_path
    File.join(smart_install_dir, "embedded", "lib", "config_guess")
  end
end