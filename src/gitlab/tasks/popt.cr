@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Popt < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "popt"

  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    update_config_guess

    command("./configure --prefix=#{install_dir}/embedded --disable-nls", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure :  Nil
    license("MIT")
    license_file("COPYING")
    version("1.16")
    source("https://ftp.osuosl.org/pub/blfs/conglomeration/popt/popt-#{version}.tar.gz",
       sha256: "e728ed296fe9f069a0e005003c3d6b2dde3d9cad453422a10d6558616d304cc8")

  end
end