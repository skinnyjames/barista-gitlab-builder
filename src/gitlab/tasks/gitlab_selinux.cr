@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabSelinux < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-selinux"

  def build : Nil
    mkdir(target, parents: true)
    sync(selinux_path, target)
  end

  def target
    File.join(install_dir, "embedded", "selinux")
  end

  def selinux_path
    File.join(resources_path, "files", "gitlab-selinux")
  end

  def configure :  Nil
    license("Apache-2.0")
    cache(false)
    virtual(true)
  end
end