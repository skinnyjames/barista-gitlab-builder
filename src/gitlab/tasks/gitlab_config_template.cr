@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabConfigTemplate < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-config-template"

  def build : Nil
    mkdir("#{install_dir}/etc", parents: true)
    sync(config_template_path, File.join(install_dir, "etc"))
  end

  def config_template_path
    File.join(resources_path, "files", "gitlab-config-template")
  end

  # skipping licensing since these are unversioned files.s
  def configure : Nil
    cache(false)
    virtual(true)
    license("Apache-2.0")
  end
end