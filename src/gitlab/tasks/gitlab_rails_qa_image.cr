# This is a virtual task that is only for running in a pipeline
#
# This task depends on docker being available in the build container
@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabRailsQaImage < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-rails-qa-image"

  dependency GitlabRails

  def build : Nil
    if in_pipeline? && project.packager.id == :deb
      command("docker build -t #{registry_qa_image(with_commit: true)} --file ./qa/Dockerfile .", chdir: rails_dir)
      command("docker tag #{registry_qa_image(with_commit: true)} #{registry_qa_image}")
      command("docker push #{registry_qa_image(with_commit: true)}")
    else
      emit("not in pipeline: skipping")
    end
  end

  def registry_qa_image(with_commit : Bool = false)
    project_name = arm? ? "#{project.name}-arm" : project.name
    registry_project = "#{project.name}-qa:#{tag(with_commit)}"

    "#{registry_path}/#{registry_project}"
  end

  def tag(with_commit : Bool = false)
    with_commit ? context.version.commit_sha : "latest"
  end

  def registry_path
    path = ENV["CI_PROJECT_PATH"]? 
    registry = ENV["CI_REGISTRY"]?

    raise "Not in CI! cannot call `registry_path`" if path.nil? || registry.nil?

    "#{registry}/#{path}"
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end