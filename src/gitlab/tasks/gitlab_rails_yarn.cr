@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabRailsYarn < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-rails-yarn"

  dependency GitlabRails

  # TODO: assets caching
  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
            .merge({
              "NODE_ENV" => "production",
              "RAILS_ENV" => "production",
              "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}",
              "USE_DB" => "false",
              "SKIP_STORAGE_VALIDATION" => "true",
              "NODE_OPTIONS" => "--max_old_space_size=#{max_old_space_size}",
              "NO_SOURCEMAPS" => "true"
            })

    command("yarn install --pure-lockfile --production", env: env, chdir: rails_dir)

    # Removing yarn:check do to buggy behavior.
    # https://github.com/yarnpkg/yarn/issues/6427#issuecomment-434499073

    command("yarn webpack-prod", env: env, chdir: rails_dir)
  end

  def max_old_space_size
    "10584"
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end