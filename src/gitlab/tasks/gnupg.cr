@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Gnupg < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gnupg"

  dependency Libassuan
  dependency Npth
  dependency Libgcrypt
  dependency Libksba
  dependency Zlib
  dependency Bzip2

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["LDFLAGS"] += " -lrt"

    config_flags = ""

    # CentOS 6 doesn't have inotify, which will raise an error
    # IN_EXCL_UNLINK undeclared. Hence disabling it explicitly.
    config_flags = "ac_cv_func_inotify_init=no" if skip_inotify

    command(configure_command(config_flags), env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure_command(extra)
    String.build do |io|
      io << "./configure --prefix=#{install_dir}/embedded "
      io << "--disable-doc --without-readline "
      io << "--disable-sqlite --disable-gnutls "
      io << "--disable-dirmngr #{extra}"
    end
  end

  def skip_inotify : Bool
    /centos/.matches?(platform.family || "") && /^6/.matches?(platform.version || "")
  end

  def configure :  Nil
    version("2.2.23")
    license("LGPL-2.1")
    license_file("COPYING.LGPL3")
    source("https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-#{version}.tar.bz2",
      sha256: "10b55e49d78b3e49f1edb58d7541ecbdad92ddaeeb885b6f486ed23d1cd1da5c")
    relative_path("gnupg-#{version}")
  end
end