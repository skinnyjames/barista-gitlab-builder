@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Prometheus < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "prometheus"

  # CentOS 7 and Amazon Linux 2 ships gzip 1.5 which does not have `-k` flag to
  # keep the files. Apply https://github.com/prometheus/prometheus/pull/11256 to
  # work around the problem.
  file("gzip_patch", "#{__DIR__}/../../patches/prometheus/gzip-k-flag.patch")

  def build : Nil
    env = { "GOPATH" => prometheus_dir, "GO111MODULE" => "on" }
    ldflags = context.prometheus_flags.ldflags(version)

    patch(file("gzip_patch"), string: true)

    command("make build", env: env)
    command("go build -tags netgo,builtinassets -ldflags '#{ldflags}' ./cmd/prometheus", env: env)
    mkdir(target, parents: true)
    copy("prometheus", target)

    license_finder
  end

  def license_finder
    cmd = String.build do |io|
      io << "license_finder report "
      io << "--decisions-file=#{dependency_decisions_path} "
      io << " --format=json --columns name version licenses texts notice "
      io << "--save=license.json"
    end
    command(cmd)
    mkdir(File.join(smart_install_dir, "licenses"), parents: true)
    copy("license.json", File.join(smart_install_dir, "licenses", "prometheus.json"))
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def prometheus_dir
    File.join(project.source_dir, "prometheus")
  end

  def configure :  Nil
    license("APACHE-2.0")
    license_file("LICENSE")
    license_file("NOTICE")
    version("2.38.0")
    source("https://github.com/prometheus/prometheus/archive/refs/tags/v#{version}.tar.gz")
    relative_path("src/github.com/prometheus/prometheus")
  end
end