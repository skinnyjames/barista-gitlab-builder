@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::ChefGem < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  file("license_file", "#{__DIR__}/../../patches/chef-gem/license/add-license-file.patch")
  file("notice_file", "#{__DIR__}/../../patches/chef-gem/license/add-notice-file.patch")

  dependency Ruby
  dependency Libffi
  dependency RbReadline
  sequence ["Gem"]

  # From Barista
  # dependency FFIGem
  # dependency GemUpdate
  # dependency ChefBin

  @@name = "chef-gem"

  def build : Nil
    patch(file("license_file"), string: true)
    patch(file("notice_file"), string: true)

    env = with_standard_compiler_flags(with_embedded_path)
    env["SSL_CERT_FILE"] = File.join(install_dir, "embedded", "ssl", "cert.pem")

    cmd = String.build do |io|
      io << "install chef --clear-sources "
      io << "-s https://packagecloud.io/cinc-project/stable "
      io << "-s https://rubygems.org "
      io << "--version '#{version}' "
      io << "--bindir '#{install_dir}/embedded/bin' "
      io << "--no-document"
    end

    bin("gem", cmd, env: env)
  end

  def configure : Nil
    version("17.10.0")
    license("Apache-2.0")
    license_file("LICENSE")
    license_file("NOTICE")
    cache(false)
  end
end
