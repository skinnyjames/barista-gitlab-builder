@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Libffi < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libffi"

  file("os_dir_patch", "#{__DIR__}/../../patches/libffi/libffi-3.2.1-disable-multi-os-directory.patch")
  
  dependency Libtool

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    flags = ["--prefix=#{install_dir}/embedded"]
    patch_3_2_1(flags, env)

    command("./configure #{flags.join(" ")}", env: env)
    command("make", env: env)
    command("make install", env: env)

    mkdir(File.join(smart_install_dir, "embedded", "include"), parents: true)
    sync("#{smart_install_dir}/embedded/lib/libffi-#{version}/include", "#{smart_install_dir}/embedded/include")
  end
  
  def patch_3_2_1(command, env)
    if version == "3.2.1"
      patch(file("os_dir_patch"), plevel: 1, env: env, string: true)
      command << "--disable-multi-os-directory"
      command << "--build=#{gcc_target}" if raspberry_pi?
    end
  end

  def configure :  Nil
    version("3.2.1")
    license("MIT")
    license_file("LICENSE")
    source("https://sourceware.org/pub/libffi/libffi-#{version}.tar.gz",
      md5: "83b89587607e3eb65c70d361f13bab43")
    # Todo: is this needed?
    relative_path("libffi-#{version}")
  end
end