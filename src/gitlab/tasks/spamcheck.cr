@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Spamcheck < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "spamcheck"

  dependency LibtensorflowLite
  dependency Unzip

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    env["GOPATH"] = "#{project.source_dir}/spamcheck"
    env["PATH"] = "#{env["PATH"]}:#{env["GOPATH"]}/bin"
    env["SSL_CERT_FILE"] = File.join(install_dir, "embedded", "ssl", "cert.pem")
    
    %w[service bin].each do |dir|
      mkdir(File.join(target, dir), parents: true)
    end

    sync(".", "#{smart_install_dir}/embedded/service/spamcheck", exclude: spamcheck_excludes)

    env["CGO_CFLAGS"] = env["CFLAGS"].dup
    env["CGO_CPPFLAGS"] = env["CPPFLAGS"].dup
    env["CGO_CXXFLAGS"] = env["CXXFLAGS"].dup
    env["CGO_LDFLAGS"] = env["LDFLAGS"].dup

    command("make build", env: env)

    mkdir("#{target}/bin", parents: true)
    copy("spamcheck", "#{target}/bin")
  end

  def spamcheck_excludes
    %w[
      _support/**/*
      build/**/*
      config/**/*
      docs/**/*
      examples/**/*
      tests/**/*
      tools/**/*
    ]
  end

  def target
    File.join(smart_install_dir, "embedded")
  end

  def configure :  Nil
    license("MIT")
    license_file("LICENSE")
    version("0.3.0")
    relative_path("src/gitlab-org/spamcheck")
    source("https://gitlab.com/gitlab-org/spamcheck/-/archive/v#{version}/spamcheck-v#{version}.tar.gz")
  end
end