@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Nginx < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "nginx"

  file("CVE_2021_3618", "#{__DIR__}/../../patches/nginx/CVE-2021-3618.patch")

  dependency NginxModuleVts
  dependency NgxSecurityHeaders
  dependency Pcre
  dependency Zlib
  dependency OpenSSL unless use_system_ssl?

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    patch(file("CVE_2021_3618"), string: true)

    command(config_cmd.join(" "), env: env)
    command("make", env: { "LD_RUN_PATH" => File.join(install_dir, "embedded", "lib") })
    command("make install", env: env)
  end

  def config_cmd
    [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--with-http_ssl_module",
      "--with-http_stub_status_module",
      "--with-http_gzip_static_module",
      "--with-http_v2_module",
      "--with-http_realip_module",
      "--with-http_sub_module",
      "--with-ipv6",
      "--with-debug",
      "--add-module=#{project.source_dir}/nginx-module-vts",
      "--add-module=#{project.source_dir}/ngx-security-headers",
      "--with-ld-opt=-L#{install_dir}/embedded/lib",
      "--with-cc-opt=\"-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include\""
    ]
  end

  def configure :  Nil
    license("BSD-2-Clause")
    license_file("LICENSE")
    version("1.20.2")
    source("http://nginx.org/download/nginx-#{version}.tar.gz")
  end
end