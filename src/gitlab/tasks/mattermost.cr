@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Mattermost < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "mattermost"

  def build : Nil
    mkdir("#{smart_install_dir}/embedded/bin", parents: true)
    mkdir("#{mattermost_dir}", parents: true)

    command("mv bin/mattermost #{smart_install_dir}/embedded/bin/mattermost")
    command("mv bin/mmctl #{smart_install_dir}/embedded/bin/mmctl")

    %w[templates i18n fonts client prepackaged_plugins].each do |item|
      mkdir("#{mattermost_dir}/#{item}", parents: true)
      sync(item, "#{mattermost_dir}/#{item}")
    end

    copy("config/config.json", "#{mattermost_dir}/config.json.template")

    block do
      File.write("#{source_dir}/GITLAB-MATTERMOST-COMPILED-LICENSE.txt", mattermost_license)
      File.write("#{mattermost_dir}/VERSION", version)
    end
  end

  def mattermost_dir
    File.join(smart_install_dir, "embedded", "service", "mattermost")
  end

  def configure :  Nil
    license("MIT with Trademark Protection")
    license_file("GITLAB-MATTERMOST-COMPILED-LICENSE.txt")
    version("7.5.1")
    source("https://releases.mattermost.com/#{version}/mattermost-team-#{version}-linux-amd64.tar.gz",
      sha256: "d934b4c9efc4008c47ddd966039e92c95e01cb5404888a16a947be112c5750d3")
  end

  def mattermost_license
    str = <<-EOH
    GitLab Mattermost Compiled License
    (MIT with Trademark Protection)
    
    **Note: this license does not cover source code, for information on source code licensing see http://www.mattermost.org/license/

    Copyright (c) 2015 Mattermost, Inc.
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software;
    The receiver of the Software will not remove or alter any product identification, trademark, copyright or other notices embedded within or appearing within or on the Software;

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    EOH
  end
end