@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Unzip < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "unzip"

  # Check in `debian/patches` and `debian/changelog` for patch files.
  file("manpages_patch", "#{__DIR__}/../../patches/unzip/01-manpages-in-section-1-not-in-section-1l.patch")
  # Replaces Debian upstream's 02 patch which is branding for who maintains
  # the final package build from this source stream.
  file("gitlab_source_patch", "#{__DIR__}/../../patches/unzip/0-gitlab-source.patch")
  file("unistd_patch", "#{__DIR__}/../../patches/unzip/03-include-unistd-for-kfreebsd.patch")
  file("pkware_patch", "#{__DIR__}/../../patches/unzip/04-handle-pkware-verification-bit.patch")
  file("fix_uid_patch", "#{__DIR__}/../../patches/unzip/05-fix-uid-gid-handling.patch")
  file("symlink_patch", "#{__DIR__}/../../patches/unzip/06-initialize-the-symlink-flag.patch")
  # Resolves CVE-2018-18384
  # See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=741384
  file("cfactorstr_patch", "#{__DIR__}/../../patches/unzip/07-increase-size-of-cfactorstr.patch")
  file("hostver_values_patch", "#{__DIR__}/../../patches/unzip/08-allow-greater-hostver-values.patch")
  file("crc_overflow_patch", "#{__DIR__}/../../patches/unzip/09-cve-2014-8139-crc-overflow.patch")
  file("compr_patch", "#{__DIR__}/../../patches/unzip/10-cve-2014-8140-test-compr-eb.patch")
  file("getzip_patch", "#{__DIR__}/../../patches/unzip/11-cve-2014-8141-getzip64data.patch")
  file("compr_patch2", "#{__DIR__}/../../patches/unzip/12-cve-2014-9636-test-compr-eb.patch")
  file("build_date_patch", "#{__DIR__}/../../patches/unzip/13-remove-build-date.patch")
  file("7696_patch", "#{__DIR__}/../../patches/unzip/14-cve-2015-7696.patch")
  file("7697_patch", "#{__DIR__}/../../patches/unzip/15-cve-2015-7697.patch")
  file("underflow_patch", "#{__DIR__}/../../patches/unzip/16-fix-integer-underflow-csiz-decrypted.patch")
  file("timestamp_patch", "#{__DIR__}/../../patches/unzip/17-restore-unix-timestamps-accurately.patch")
  file("buffer_overflow_patch", "#{__DIR__}/../../patches/unzip/18-cve-2014-9913-unzip-buffer-overflow.patch")
  file("zip_buffer_overflow_patch", "#{__DIR__}/../../patches/unzip/19-cve-2016-9844-zipinfo-buffer-overflow.patch")
  file("buffer_overflow_patch2", "#{__DIR__}/../../patches/unzip/20-cve-2018-1000035-unzip-buffer-overflow.patch")
  file("big_files_patch", "#{__DIR__}/../../patches/unzip/21-fix-warning-messages-on-big-files.patch")
  file("undefer_input_patch", "#{__DIR__}/../../patches/unzip/22-cve-2019-13232-fix-bug-in-undefer-input.patch")
  file("zip_bomb_patch", "#{__DIR__}/../../patches/unzip/23-cve-2019-13232-zip-bomb-with-overlapped-entries.patch")
  file("central_directory_patch", "#{__DIR__}/../../patches/unzip/24-cve-2019-13232-do-not-raise-alert-for-misplaced-central-directory.patch")
  file("uzbunzip2_patch", "#{__DIR__}/../../patches/unzip/25-cve-2019-13232-fix-bug-in-uzbunzip2.patch")
  file("uzinflate_patch", "#{__DIR__}/../../patches/unzip/26-cve-2019-13232-fix-bug-in-uzinflate.patch")
  file("test_errors_patch", "#{__DIR__}/../../patches/unzip/27-zipgrep-avoid-test-errors.patch")
  file("0530_patch", "#{__DIR__}/../../patches/unzip/28-cve-2022-0529-and-cve-2022-0530.patch")


  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    patch_keys.each do |key|
      patch(file(key), string: true)
    end

    command("make -f unix/Makefile clean", env: env)
    command("make -f unix/Makefile generic", env: env)
    command("make -f unix/Makefile prefix=#{smart_install_dir}/embedded install", env: env)
  end

  def patch_keys
    [
      "manpages_patch",
      "gitlab_source_patch",
      "unistd_patch",
      "pkware_patch",
      "fix_uid_patch",
      "symlink_patch",
      "cfactorstr_patch",
      "hostver_values_patch",
      "crc_overflow_patch",
      "compr_patch",
      "getzip_patch",
      "compr_patch2",
      "build_date_patch",
      "7696_patch",
      "7697_patch",
      "underflow_patch",
      "timestamp_patch",
      "buffer_overflow_patch",
      "zip_buffer_overflow_patch",
      "buffer_overflow_patch2",
      "big_files_patch",
      "undefer_input_patch",
      "zip_bomb_patch",
      "central_directory_patch",
      "uzbunzip2_patch",
      "uzinflate_patch",
      "test_errors_patch",
      "0530_patch"
    ]
  end

  def configure :  Nil
    version("6.0.27")
    license("Info-ZIP")
    license_file("LICENSE")
    preserve_symlinks(false)
    source("https://downloads.sourceforge.net/project/infozip/UnZip%206.x%20%28latest%29/UnZip%206.0/unzip60.tar.gz",
      sha256: "036d96991646d0449ed0aa952e4fbe21b476ce994abc276e49d30e686708bd37")
  end
end