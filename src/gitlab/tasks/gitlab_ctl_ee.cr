@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
class Gitlab::Tasks::GitlabCtlEE < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  dependency GitlabCtl

  @@name = "gitlab-ctl-ee"

  def build : Nil
    mkdir(target, parents: true)
    sync(commands_path, target)
  end

  def target
    File.join(install_dir, "embedded", "service", "omnibus-ctl-ee")
  end

  def commands_path
    File.join(resources_path, "files", "gitlab-ctl-commands-ee")
  end

  def configure : Nil
    license("Apache-2.0")
    cache(false)
    virtual(true)
  end
end