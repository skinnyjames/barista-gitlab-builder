# This is a virtual task that is only for running in a pipeline
#
# This task depends on docker being available in the build container
@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::PackageAndPush < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "package-and-push"

  dependency GitlabRailsFinish
  dependency GitlabRailsQaImage

  # Only debian packages are currently supported for
  # the docker container export
  def build : Nil
    if in_pipeline? && project.packager.id == :deb
      build_package
      save_release_file

      command("mv *.deb #{ci_project_dir}/package/gitlab.deb", chdir: File.join(barista_dir, "package"))
      command("docker build -t #{registry_image(with_commit: true)} .", chdir: "#{ci_project_dir}/package")
      command("docker tag #{registry_image(with_commit: true)} #{registry_image}")
      command("docker push #{registry_image(with_commit: true)}")

      # push latest tags for the app/qa images.
      # the qa:latest tag was buit in GitlabRailsQaImage
      # but we want to ensure the app image push succeeds first.
      command("docker push #{registry_image}")
      command("docker push #{registry_qa_image}")
    else
      emit("Only building #{project.packager.id} to #{barista_dir}/package\n This will not be exported anywhere.")

      build_package
    end
  end

  def save_release_file
    block do 
      data = <<-EOH
      RELEASE_PACKAGE=#{project.name}
      RELEASE_VERSION=#{context.version.semver_version}
      EOH

      Dir.cd("#{ci_project_dir}/package") do
        File.write("RELEASE", data)
      end
    end
  end

  def build_package
    block do
      project.packager
        .forward_output(&on_output)
        .forward_error(&on_error)
        .run
      
      project.packager.query
    end
  end

  def registry_qa_image(with_commit : Bool = false)
    project_name = arm? ? "#{project.name}-arm" : project.name
    registry_project = "#{project.name}-qa:#{tag(with_commit)}"

    "#{registry_path}/#{registry_project}"
  end

  def registry_image(with_commit : Bool = false)
    project_name = arm? ? "#{project.name}-arm" : project.name
    registry_project = "#{project.name}:#{tag(with_commit)}"

    "#{registry_path}/#{registry_project}"
  end

  def tag(with_commit : Bool = false)
    with_commit ? context.version.commit_sha : "latest"
  end

  def registry_path : String
    path = ENV["CI_PROJECT_PATH"]? 
    registry = ENV["CI_REGISTRY"]?

    raise "Not in CI! cannot call `registry_path`" if path.nil? || registry.nil?

    "#{registry}/#{path}"
  end

  def package_path
    File.join(barista_dir, "package")
  end

  def ci_project_dir : String
    ENV["CI_PROJECT_DIR"]
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end
