@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::NodeExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "node-exporter"

  def build : Nil
    env = {
      "GOPATH" => "#{project.source_dir}/node_exporter",
      "CGO_ENABLED" => "0", # Details: https://github.com/prometheus/node_exporter/issues/870
      "GO111MODULE" => "on"
    }

    mkdir("#{smart_install_dir}/embedded/bin", parents: true)
    command("go build -ldflags '#{context.prometheus_flags.ldflags(version)}'", env: env)
    copy("node_exporter", "#{smart_install_dir}/embedded/bin")

    license_finder
  end

  def license_finder
    cmd = String.build do |io|
      io << "license_finder report "
      io << "--enabled-package-managers godep gomodules "
      io << "--decisions-file=#{dependency_decisions_path} "
      io << " --format=json --columns name version licenses texts notice "
      io << "--save=license.json"
    end
    command(cmd)
    mkdir(File.join(smart_install_dir, "licenses"), parents: true)
    copy("license.json", File.join(smart_install_dir, "licenses", "node-exporter.json"))
  end

  def configure :  Nil
    license("APACHE-2.0")
    license_file("LICENSE")
    license_file("NOTICE")
    version("1.4.0")
    source("https://github.com/prometheus/node_exporter/archive/refs/tags/v#{version}.tar.gz")
    relative_path("src/github.com/prometheus/node_exporter")
  end
end