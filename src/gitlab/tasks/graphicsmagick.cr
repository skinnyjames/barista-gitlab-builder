@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Graphicsmagick < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "graphicsmagick"

  dependency Libpng
  dependency LibjpegTurbo
  dependency Libtiff
  dependency Zlib

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command(configure_command.join(" "), env: env)
    command("make -j 4", env: env)
    command("make install", env: env)
  end

  def configure_command
    [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-openmp",
      "--without-magick-plus-plus",
      "--with-perl=no",
      "--without-bzlib",
      "--without-dps",
      "--without-fpx",
      "--without-gslib",
      "--without-jbig",
      "--without-webp",
      "--without-jp2",
      "--without-lcms2",
      "--without-trio",
      "--without-ttf",
      "--without-umem",
      "--without-wmf",
      "--without-xml",
      "--without-x",
      "--with-tiff=yes",
      "--with-lzma=yes",
      "--with-jpeg=yes",
      "--with-zlib=yes",
      "--with-png=yes",
      "--with-sysroot=#{install_dir}/embedded"
    ]
  end

  def configure :  Nil
    license("MIT")
    license_file("Copyright.txt")
    version("1.3.36")
    source("https://sourceforge.net/projects/graphicsmagick/files/graphicsmagick/#{version}/GraphicsMagick-#{version}.tar.gz",
      sha256: "1e6723c48c4abbb31197fadf8396b2d579d97e197123edc70a4f057f0533d563")
    relative_path("GraphicsMagick-#{version}")
  end
end