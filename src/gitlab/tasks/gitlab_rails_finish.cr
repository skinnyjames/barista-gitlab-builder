@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabRailsFinish < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  file("bundle_exec_wrapper", "#{__DIR__}/../../templates/gitlab-rails/bundle_exec_wrapper.hbs")
  file("gem_license_generator", "#{__DIR__}/../../templates/gitlab-rails/gem_license_generator.hbs")
  file("rake_backup_wrapper", "#{__DIR__}/../../templates/gitlab-rails/rake_backup_wrapper.hbs")

  dependency GitlabRailsTextCompile
  dependency GitlabRailsAssetsCompile

  @@name = "gitlab-rails-finish"

  def build : Nil
    write_revision
    write_installation_type

    delete_special_files

    # Delete uncompressed sourcemaps
    command("find public/assets/webpack -name '*.map' -type f -print -delete", chdir: rails_dir)
    
    # Delete all .gem archives
    command("find #{install_dir} -name '*.gem' -type f -print -delete")

    # Delete docs
    command("find #{install_dir}/embedded/lib/ruby/gems -name 'doc' -type d -print -exec rm -r {} +")

    copy("db/structure.sql", "db/structure.sql.bundled", chdir: rails_dir)
    copy("ee/db/geo/structure.sql", "ee/db/geo/structure.sql.bundled", chdir: rails_dir) if context.version.ee?

    mkdir(target, parents: true)
    sync(rails_dir, target, exclude: %w[
      .git
      .gitignore
      spec/**/*
      ee/spec/**/*
      features/**/*
      qa/**/*
      rubocop/**/*
      app/assets/**/*
      vendor/assets/**/*
      ee/app/assets/**/*
      workhorse/**/*
    ])

    write_templates

    project.license_collector.write_summary

    # TODO: fix this
    # bin("ruby", "#{install_dir}/embedded/bin/gitlab-gem-license-generator")
    # command("rm #{install_dir}/embedded/bin/gitlab-gem/license-generator")
  end

  def target
    File.join(install_dir, "embedded", "service", "gitlab-rails")
  end

  def write_templates
    # ensure directories exist
    mkdir("#{install_dir}/bin", parents: true)
    mkdir("#{install_dir}/embedded/bin", parents: true)

    template(
      src: file("bundle_exec_wrapper"), 
      dest: "#{install_dir}/bin/gitlab-rake",
      mode: mode,
      vars: { "command" => "rake \"$@\"", "install_dir" => install_dir },
      string: true
    )

    template(
      src: file("bundle_exec_wrapper"), 
      dest: "#{install_dir}/bin/gitlab-rails",
      mode: mode,
      vars: { "command" => "rails \"$@\"", "install_dir" => install_dir },
      string: true
    )

    template(
      src: file("bundle_exec_wrapper"), 
      dest: "#{install_dir}/bin/gitlab-ruby",
      mode: mode,
      vars: { "command" => "ruby \"$@\"", "install_dir" => install_dir },
      string: true
    )

    template(
      src: file("rake_backup_wrapper"), 
      dest: "#{install_dir}/bin/gitlab-backup",
      mode: mode,
      vars: { "install_dir" => install_dir, "while_arg" => "{#}" },
      string: true
    )
    
    template(
      src: file("gem_license_generator"), 
      dest: "#{install_dir}/embedded/bin/gitlab-gem-license-generator",
      mode: mode,
      vars: { "license_file" => combined_licenses_file, "install_dir" => install_dir },
      string: true
    )
  end

  # TODO: figure this one out
  def combined_licenses_file
    ""
  end

  def delete_special_files
    special_files.each do |file|
      command("rm -Rf #{file}", chdir: rails_dir)
    end
  end

  def mode
    File::Permissions.new(0o755)
  end

  def special_files
    %w[
      config/gitlab.yml config/database.yml config/secrets.yml 
      .secret .gitlab_shell_secret .gitlab_workhorse_secret .gitlab_pages_secret
      .gitlab_kas_secret .gitlab_incoming_email_secret .gitlab_service_desk_email_secret
      log tmp public/uploads
    ]
  end

  def write_revision
    File.write("#{rails_dir}/REVISION", context.version.commit_sha)
  end

  def write_installation_type
    File.write("#{rails_dir}/INSTALLATION_TYPE", "omnibus_gitlab")
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end