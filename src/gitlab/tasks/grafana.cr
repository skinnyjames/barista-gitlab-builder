# @[Barista::BelongsTo(Gitlab::GitlabFIPS)]
# @[Barista::BelongsTo(Gitlab::GitlabEE)]
# @[Barista::BelongsTo(Gitlab::GitlabCE)]
# Temporarily removing this task from the registry.
class Gitlab::Tasks::Grafana < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "grafana"

  dependency Cacerts

  file("oauth_patch", "#{__DIR__}/../../patches/grafana/1-cve-2022-31107-oauth-vulnerability.patch")

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    env["CYPRESS_INSTALL_BINARY"] = "0"
    env["NODE_OPTIONS"] = "--max_old_space_size=#{max_old_space_size}"

    # make node_modules needs a cert 
    env["GIT_SSL_CAINFO"] = File.join(install_dir, "embedded", "ssl", "cert.pem")

    patch(file("oauth_patch"), string: true)

    # build backend
    command("make build-go", env: env)
    build_frontend(env)

    mkdir(target, parents: true)

    # copy binaries
    command("cp bin/linux-*/grafana-server #{target}/.")
    command("cp bin/linus-*/grafana-cli #{target}/.")

    # copy assets
    mkdir("#{service_dir}/public", parents: true)
    sync("public", "#{service_dir}/public")

    # copy configuration
    mkdir("#{service_dir}/conf", parents: true)
    copy("conf/defaults.ini", File.join(service_dir, "confg", "defaults.ini"))
  end

  def build_frontend(env)
    # Yarn errors out a lot in this build.
    # remove the lockfile and try to rebuild it.
    # command("rm yarn.lock", env: env)
    # command("yarn cache clean")
    # command("yarn install --verbose", env: env)
    command("make node_modules", env: env)
    assets_env = { "NODE_ENV" => "production" }
    command("make build-js", env: assets_env)
  end

  def max_old_space_size
    "10584"
  end

  def service_dir
    File.join(smart_install_dir, "embedded", "service", "grafana")
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def sha
   {
    "amd64" => "78491e7c07a3f2d810a55256df526d66b7f3d6ee3628ae5ab3862b81838d7852",
    "arm64" => "9c7ed2c9ccd2fe4844bd8e02cc11beb9eedd87345678fc944e55610249e83cb4",
    "armv7" => "95039bd8c239b93819a381b37bcdf6c3d5515b189b748ea84bee49b692be38c7"
   }[arch]
  end

  def arch
    if raspberry_pi?
      "armv7"
    elsif /aarch64/.matches?(kernel.machine || "")
      "arm64"
    else
      "amd64"
    end
  end

  def configure :  Nil
    license("AGPLv3") # omnibus says APACHE-2.0
    license_file("LICENSE")
    license_file("NOTICE.md")
    version("7.5.17")
    source("https://github.com/grafana/grafana/archive/refs/tags/v#{version}.tar.gz")
    # build from source
    # source("https://dl.grafana.com/oss/release/grafana-#{version}.linux-#{arch}.tar.gz", sha256: sha)
  end
end