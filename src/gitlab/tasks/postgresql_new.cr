@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::PostgresqlNew < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "postgresql_new"

  file("no_docs_patch", "#{__DIR__}/../../patches/postgresql_new/no_docs.patch")

  dependency Zlib
  dependency OpenSSL unless use_system_ssl?
  dependency Libedit
  dependency LibosspUuid
  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["CFLAGS"] += " -fno-omit-frame-pointer"

    update_config_guess(target: "config")
    patch(file("no_docs_patch"), target: "GNUmakefile.in", string: true)

    command([
      "./configure",
      "--prefix=#{prefix}",
      "--with-libedit-preferred",
      "--with-openssl",
      "--with-uuid=ossp"
    ].join(" "), env: env)

    command("make world", env: env)
    command("make install-world", env: env)
    mkdir("#{smart_install_dir}/embedded/bin", parents: true)
    mkdir("#{smart_install_dir}/embedded/lib", parents: true)

    # When using a cache, postgresql is built with
    # DESTDIR=<smart_install_dir> and symlinks are copied as-is.
    # create the links to where we want it to resolve.
    link("#{prefix}/lib/#{libpq}", "embedded/lib/#{libpq}", chdir: smart_install_dir)

    # NOTE: There are several dependencies which require these files in these
    # locations and have dependency on `postgresql_new`. So when this block is
    # changed to be in the `postgresql` software definition for default PG
    # version changes, change those dependencies to `postgresql`.
    block "Linking binary files" do
      Dir.cd("#{smart_install_dir}/embedded/bin") do
        Dir.glob("#{smart_prefix}/bin/*").each do |bin_file|
          base = File.basename(bin_file)
          FileUtils.ln_s("../postgresql/#{major_version}/bin/#{base}", base)
        end
      end
    end
  end

  def smart_prefix
    File.join(smart_install_dir, "embedded", "postgresql", major_version)
  end

  def prefix
    File.join(install_dir, "embedded", "postgresql", major_version)
  end

  def major_version
    "13"
  end

  def libpq
    "libpq.so.5"
  end

  def configure :  Nil
    license("PostgreSQL")
    license_file("COPYRIGHT")
    version("13.8")
    source("https://ftp.postgresql.org/pub/source/v#{version}/postgresql-#{version}.tar.bz2")
    project.exclude("embedded/postgresql/#{major_version}/include")
    project.exclude("embedded/postgresql/#{major_version}/lib/*.a")
    project.exclude("embedded/postgresql/#{major_version}/lib/pgxs")
    project.exclude("embedded/postgresql/#{major_version}/lib/pkgconfig")
  end
end