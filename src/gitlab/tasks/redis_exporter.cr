@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::RedisExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "redis-exporter"
  
  def build : Nil
    env = { "GOPATH" => "#{project.source_dir}/redis-exporter", "GO111MODULE" => "on" }

    command("go build -ldflags '#{ldflags.join(" ")}'", env: env)
    mkdir(target, parents: true)
    copy("redis_exporter", target)

    license_finder
  end

  def license_finder
    cmd = String.build do |io|
      io << "license_finder report "
      io << "--enabled-package-managers godep gomodules "
      io << "--decisions-file=#{dependency_decisions_path} "
      io << " --format=json --columns name version licenses texts notice "
      io << "--save=license.json"
    end
    command(cmd)
    mkdir(File.join(smart_install_dir, "licenses"), parents: true)
    copy("license.json", File.join(smart_install_dir, "licenses", "redis-exporter.json"))
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end
  
  def ldflags
    [
      "-X main.BuildVersion=#{version}",
      "-X main.BuildDate=''",
      "-X main.BuildCommitSha=''",
      "-s",
      "-w"
    ]
  end

  def configure :  Nil
    license("MIT")
    license_file("LICENSE")
    relative_path("src/github.com/oliver006/redis_exporter")
    version("1.44.0")
    source("https://github.com/oliver006/redis_exporter/archive/refs/tags/v#{version}.tar.gz")
  end
end