@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GoCrond < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "go-crond"

  def build : Nil
    env = {
      "GOPATH" => File.join(project.source_dir, name)
    }

    command("make build", env: env)
    
    mkdir(target, parents: true)
    copy("go-crond", target)
  end

  def target
    File.join(install_dir, "embedded", "bin")
  end

  def configure :  Nil
    version("22.9.1")
    license("BSD-2-Clause")
    license_file("LICENSE")
    relative_path("src/github.com/webdevops/go-crond")
    source("https://github.com/webdevops/go-crond/archive/refs/tags/#{version}.tar.gz")
  end
end