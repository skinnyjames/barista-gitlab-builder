@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::LibgpgError < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libgpg-error"

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("./configure --prefix=#{install_dir}/embedded --disable-doc", env: env)
    command("make -j 3", env: env)
    command("make install", env: env)
  end

  def configure :  Nil
    version("1.39")
    license("LGPL-2.1")
    license_file("COPYING.LIB")
    source("https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-#{version}.tar.bz2",
      sha256: "4a836edcae592094ef1c5a4834908f44986ab2b82e0824a0344b49df8cdb298f")
    
    # is this needed?
    # relative_path("libgpg-error-#{version}")

    project.exclude("embedded/bin/gpg-error-config")
  end
end