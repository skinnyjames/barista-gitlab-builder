@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Pgbouncer < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "pgbouncer"

  dependency Libevent
  dependency OpenSSL unless use_system_ssl?

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    cmd = "./configure --prefix=#{prefix} --with-libevent=#{prefix}"
    cmd += " --with-openssl=#{prefix}" unless use_system_ssl?

    command(cmd, env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def prefix
    File.join(install_dir, "embedded")
  end

  def configure :  Nil
    license("ISC")
    license_file("COPYRIGHT")
    version("1.12.0")
    source("https://www.pgbouncer.org/downloads/files/#{version}/pgbouncer-#{version}.tar.gz", 
      sha256: "1b3c6564376cafa0da98df3520f0e932bb2aebaf9a95ca5b9fa461e9eb7b273e")
  end
end