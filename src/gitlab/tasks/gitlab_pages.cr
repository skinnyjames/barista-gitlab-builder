@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabPages < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-pages"
  
  def build : Nil
    env = { "GOPATH" => "#{project.source_dir}/gitlab-pages" }
    env["FIPS_MODE"] = "1" if use_system_ssl?

    command("make gitlab-pages", env: env)
    mkdir(target, parents: true)
    command("mv gitlab-pages #{target}/gitlab-pages")
    
    find_licenses
  end

  # TODO find licenses
  # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/config/software/gitlab-pages.rb#L45
  def find_licenses
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure : Nil
    version(context.version.gitlab_pages_version)
    license("MIT")
    license_file("LICENSE")

    source("https://gitlab.com/gitlab-org/gitlab-pages/-/archive/#{version}/gitlab-pages-#{version}.tar.gz")
    relative_path("src/gitlab.com/gitlab-org/gitlab-pages")
  end
end