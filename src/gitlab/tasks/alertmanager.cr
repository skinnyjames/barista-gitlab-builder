
@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Alertmanager < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "alertmanager"

  def build : Nil
    env = {
      "GOPATH" => File.join(project.source_dir, "alertmanager"),
      "GO111MODULE" => "on"
    }

    ldflags = context.prometheus_flags.ldflags(version)
    target = File.join(smart_install_dir, "embedded", "bin")

    command("go build -ldflags '#{ldflags}' ./cmd/alertmanager", env: env)
    mkdir(target, parents: true)
    copy("alertmanager", target)
  end

  def configure : Nil
    version("0.23.0")
    license("APACHE-2.0")
    license_file("LICENSE")
    license_file("NOTICE")
    relative_path(File.join("src", "github.com", "prometheus", "alertmanager"))
    source( "https://github.com/prometheus/alertmanager/archive/refs/tags/v#{version}.tar.gz")
  end
end