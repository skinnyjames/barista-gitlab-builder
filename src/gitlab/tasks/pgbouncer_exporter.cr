@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::PgbouncerExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "pgbouncer-exporter"

  def build : Nil
    env = {
      "GOPATH" => "#{project.source_dir}/pgbouncer_exporter",
      "GO111MODULE" => "on"
    }

    command("go build -ldflags '#{context.prometheus_flags.ldflags(version)}'", env: env)
    mkdir(target, parents: true)
    copy("pgbouncer_exporter", target)

    license_finder
  end

  def license_finder
    cmd = String.build do |io|
      io << "license_finder report "
      io << "--enabled-package-managers godep gomodules "
      io << "--decisions-file=#{dependency_decisions_path} "
      io << " --format=json --columns name version licenses texts notice "
      io << "--save=license.json"
    end
    command(cmd)
    mkdir(File.join(smart_install_dir, "licenses"), parents: true)
    copy("license.json", File.join(smart_install_dir, "licenses", "pgbouncer-exporter.json"))
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure :  Nil
    license("MIT")
    license_file("LICENSE")
    version("0.5.1")
    relative_path("src/github.com/prometheus-community/pgbouncer_exporter")
    source("https://github.com/prometheus-community/pgbouncer_exporter/archive/refs/tags/v#{version}.tar.gz")
  end
end