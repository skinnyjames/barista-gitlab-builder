@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::NgxSecurityHeaders < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "ngx-security-headers"

  def build : Nil
    emit("downloaded ngx-security-headers")
  end

  def configure :  Nil
    version("0.0.9")
    license("BSD-2-Clause")
    license_file("LICENSE")
    source("https://github.com/GetPageSpeed/ngx_security_headers/archive/refs/tags/#{version}.tar.gz")
    cache(false)
  end
end