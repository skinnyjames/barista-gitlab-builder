@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GrafanaDashboards < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "grafana-dashboards"

  # temporary change
  dependency GrafanaSimple
  # TODO: switch back to the Grafana task
  # dependency Grafana

  def build : Nil
    mkdir(target, parents: true)
    sync("omnibus", target)
  end

  def target
    File.join(smart_install_dir, "embedded", "service", "grafana-dashboards")
  end

  def configure :  Nil
    version("1.9.0")
    source("https://gitlab.com/gitlab-org/grafana-dashboards/-/archive/v1.9.0/grafana-dashboards-v#{version}.tar.gz")
    license("MIT")
    license_file("LICENSE")
  end
end