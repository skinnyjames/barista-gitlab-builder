@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::Ncurses < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "ncurses"

  dependency ConfigGuess

  ########################################################################
  #
  # wide-character support:
  # Ruby 1.9 optimistically builds against libncursesw for UTF-8
  # support. In order to prevent Ruby from linking against a
  # package-installed version of ncursesw, we build wide-character
  # support into ncurses with the "--enable-widec" configure parameter.
  # To support other applications and libraries that still try to link
  # against libncurses, we also have to create non-wide libraries.
  #
  # The methods below are adapted from:
  # http://www.linuxfromscratch.org/lfs/view/development/chapter06/ncurses.html
  #
  ########################################################################
  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env.delete("CPPFLAGS")

    update_config_guess

    cmd = [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--enable-overwrite",
      "--with-shared",
      "--with-termlib",
      "--without-ada",
      "--without-cxx-binding",
      "--without-debug",
      "--without-manpages"
    ]

    command(cmd.join(" "), env: env)
    command("make", env: env)
    command("make install", env: env)

    # Build non-wide-character libraries
    command("make distclean", env: env)
    cmd << "--enable-widec"

    command(cmd.join(" "), env: env)
    command("make", env: env)
    # Installing the non-wide libraries will also install the non-wide
    # binaries, which doesn't happen to be a problem since we don't
    # utilize the ncurses binaries in private-chef (or oss chef)
    command("make install", env: env)
  end

  def configure :  Nil
    version("6.3")
    # 6.3 with patch 20221217
    source("https://gitlab.com/gitlab-org/build/omnibus-mirror/ncurses/-/archive/38e60f5f7931446480f1a7ca707c370ec365ba47/ncurses-38e60f5f7931446480f1a7ca707c370ec365ba47.tar.gz")
    license("MIT")
    license_file("COPYING")

    project.exclude("embedded/bin/ncurses6-config")
    project.exclude("embedded/bin/ncursesw6-config")
  end
end