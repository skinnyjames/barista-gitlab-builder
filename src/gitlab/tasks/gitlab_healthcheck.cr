@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::GitlabHealthcheck < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-healthcheck"

  def build : Nil
    mkdir(File.join(smart_install_dir, "bin"), parents: true)

    block do
      File.open(File.join(smart_install_dir, "bin", "gitlab-healthcheck"), "w") { |file| file.print(script) }
    end

    command("chmod 755 #{smart_install_dir}/bin/gitlab-healthcheck")
  end

  def script
    str = <<-EOH
    #!/bin/sh
    
    error_echo()
    {
      echo "$1" 2>& 1
    }

    gitlab_healthcheck_rc='/opt/gitlab/etc/gitlab-healthcheck-rc'


    if ! [ -f ${gitlab_healthcheck_rc} ] ; then
      exit 1
    fi

    . ${gitlab_healthcheck_rc}

    exec /opt/gitlab/embedded/bin/curl $@ ${flags} ${url}
    EOH

    str
  end

  def configure : Nil
    version(Digest::MD5.new.file(__FILE__).final.hexstring)
    license("Apache-2.0")
  end
end