@[Barista::BelongsTo(Gitlab::GitlabFIPS)]
@[Barista::BelongsTo(Gitlab::GitlabEE)]
@[Barista::BelongsTo(Gitlab::GitlabCE)]
class Gitlab::Tasks::MixlibLog < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "mixlib-log"

  file("license_patch", "#{__DIR__}/../../patches/mixlib-log/license/add-license-file.patch")
  file("notice_patch", "#{__DIR__}/../../patches/mixlib-log/license/add-notice-file.patch")

  dependency Ruby
  sequence ["Gem"]

  def build : Nil
    patch(file("license_patch"), string: true)
    patch(file("notice_patch"), string: true)

    env = with_standard_compiler_flags(with_embedded_path)

    bin("gem", "install mixlib-log --version '#{version}' --bindir '#{install_dir}/embedded/bin' --no-document", env: env)
  end

  def configure :  Nil
    license("Apache-2.0")
    license_file("LICENSE")
    license_file("NOTICE")
    version("3.0.9")
    cache(false)
  end
end