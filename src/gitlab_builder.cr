require "barista"
require "./gitlab/platform_helper"
require "./gitlab/common"
require "./gitlab/task_data"
require "./gitlab/task_helpers"
require "./gitlab/projects/**"
require "./gitlab/tasks/**"
require "./gitlab/cli_commands/**"

project_map = [Gitlab::GitlabCE, Gitlab::GitlabEE, Gitlab::GitlabFIPS, Gitlab::GitlabTest].reduce({} of String => Gitlab::Common) do |memo, klass|
  p = klass.new
  memo[p.name] = p.as(Gitlab::Common)
  memo
end

console = ACON::Application.new("gitlab-builder")
console.add(Gitlab::CliCommands::BuildProject.new(project_map))
console.add(Gitlab::CliCommands::ProjectTasks.new(project_map))
console.run