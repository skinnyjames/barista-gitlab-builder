require "../spec_helper"
require "../support/provider"
require "./project/*"

module Internal
  describe "Provider" do
    it "mocks the required files" do
      provider = Support::Provider.new("#{downloads_path}/internal/serve", "#{__DIR__}/project/provides.yml")    

      project = Provides.new

      begin
        project.build(provider)
      rescue ex
        ex.should eq(nil)
      ensure
        provider.restore_env
      end
    end
  end
end