require "spec"
require "webmock"
require "../src/gitlab/**"
require "./support/*"

include FileUtils

module Util
  @@clean = "true"

  def self.clean?
    @@clean.try(&.==("false")) || true
  end

  def self.clean=(val)
    @@clean = val
  end
end

GITLAB_EE_PROJECT = Gitlab::GitlabEE.new
  
def fixtures_path
  File.join(__DIR__, "fixtures")
end

def downloads_path
  File.join(fixtures_path, "downloads")
end

def clean
  rm_r(downloads_path) if Dir.exists?(downloads_path)

  mkdir_p(fixtures_path)
  mkdir_p(downloads_path)
end

def ee_project_path
  File.join(downloads_path, "mount-gitlab-ee")
end

def task(name)
  project.registry[name]
end

def project
  GITLAB_EE_PROJECT
end

def build_version
  "1.2.3"
end

def build_version_major
  "1"
end

def build_version_minor
  "1.2"
end

def file_should_contain(path, arr : Array(String))
  file = File.read(path)
  arr.each do |string|
    file.should contain(string)
  end
end

def file_should_contain(path, string : String)
  File.read(path).should contain(string)
end

def file_should_exist(path)
  File.exists?(path).should eq(true)
end

def build_project
  provider = Support::Provider.new("#{ee_project_path}/barista/serve", "#{__DIR__}/support/provides.yml")
  finder = Gitlab::MockVersion.new("1", ee: true)

  project.build_version(build_version)
  project.install_dir("#{ee_project_path}/gitlab")
  project.barista_dir("#{ee_project_path}/barista")
  project.cache(false)

  begin
    project.test_build(finder, provider)
  ensure
    provider.restore_env
  end
end

Spec.before_suite do
  clean
  build_project
end

Spec.after_suite do
  clean unless ENV["CLEAN"]? == "false"
end