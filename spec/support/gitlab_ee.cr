require "barista"
require "./mocks/version"
require "./mocks/os"
require "colorize"

module Gitlab
  class GitlabEE < Barista::Project
    include FileUtils
    
    def initialize
      super
    end

    def test_build(finder : VersionFinder, provider : Support::Provider)
      mkdir_p("#{barista_dir}/serve")

      context = TaskData.new(version: finder)
      colors = Barista::ColorIterator.new

      tasks.each do |klass|
        logger = Barista::RichLogger.new(name: klass.name, color: colors.next)

        task = klass.new(self, context: context)
        
        task.on_output do |str| 
          puts "#{task.name}> #{str.colorize(:blue)}"
        end

        task.on_error do |str|
          puts "#{task.name}> #{str.colorize(:red)}"
        end
      end

      orchestration = orchestrator(workers: 5)

      orchestration.on_run_start do
        provider.prepare
      end

      orchestration.on_task_start do |name|
        puts "Starting #{name}.."
        task = registry[name]

        provider.prepare(task)
      end

      orchestration.on_task_failed do |task, ex|
        puts "#{task} raised: #{ex}"
      end
      
      orchestration.on_task_finished do |task|
        puts "Task finished: #{task}"
      end

      orchestration.on_unblocked do |info|
        str = <<-EOH
        Unblocked #{info.unblocked.join(",")}
        Building #{info.building.join(",")}
        Blocked #{info.blocked.join(", ")}
        Active Seq #{info.active_sequences.join(", ")}
        EOH
        puts str.colorize(:yellow)
      end

      orchestration.execute
    end
  end
end