require "yaml"
require "http"
require "file_utils"
require "barista"

module Support
  class Provider
    include FileUtils
    
    getter :state, :path, :binary_data, :port, :allowed, :env

    @env = {} of String => String
    @allowed = [] of Tuple(String, String)

    def initialize(@path : String, file : String, *, @port : Int32 = 3002)
      content = File.read(file)
      @state = YAML.parse(content)
      @binary_data = File.read("#{__DIR__}/provider/binary.sh")
    end

    def register_symlink(name)
      binary_path = `which #{name}`.strip
      raise "Binary #{name} not found" if binary_path =~ /not found/
      allowed << { binary_path, name }
    end

    # Used to prepare the system
    def prepare
      mkdir_p("#{path}/system")
      register_needed_binaries
      
      state["system"]?.try do |sys|
        sys["binaries"]?.try do |binaries|
          binaries.as_a.each do |binary|
            mock_system_binary(binary)
          end
        end
        sys["allows"]?.try do |binaries|
          binaries.as_a.each do |binary|
            register_symlink(binary.as_s)
          end
        end
      end

      allowed.each do |realpath, command|
        ln_s(realpath, "#{path}/system/#{command}")
      end

      update_permissions("#{path}/system")
      
      @env = reset_env
    end

    def prepare(task : Barista::Behaviors::Omnibus::Task)
      begin
        defn = state["tasks"].as_h[task.name]?.try(&.as_h)

        update_commands(defn, task)

        return if defn.nil?

        mkdir_p(task_path(task))

        defn["provides"]?.try do |provides|
          prepare_providers(provides.as_h, task)
        end

        defn["installs"]?.try do |installs|
          prepare_installs(installs.as_h, task)
        end

        defn["mocks"]?.try do |mocks|
          prepare_mocks(mocks.as_a, task)
        end

        update_permissions(task_path(task))
        update_permissions(task.install_dir)

        mock_task_file(task)
      rescue ex
        puts "Error in Provider#prepare for #{task.name}> raised #{ex}"
        exit 1
      end
    end

    def update_permissions(path)
      Dir.glob("#{path}/**/*").each do |file|
        if File.file?(file) && !File.symlink?(file)
          File.chmod(file, File::Permissions.new(0o755))
        end
      end
    end

    # providers are things that get downloaded.
    def prepare_providers(defn, task)
      defn["license_files"]?.try do |files|
        files.as_a.each do |name|
          File.write(file_path(task, name.as_s), "A LICENSE")
        end
      end

      defn["binaries"]?.try do |binaries|
        binaries.as_a.each do |name|
          File.write(file_path(task, name.as_s), binary_data)
        end
      end

      defn["files"]?.try do |files|
        files.as_a.each do |file|
          hash = file.as_h
          File.write(file_path(task, hash["name"].as_s), hash["content"].as_s)
        end
      end
    end

    def prepare_mocks(defn, task)
      defn.each do |mock|
        if mock.as_h["type"] == "api"
          url = mock.as_h["url"].as_s
          content = mock.as_h["content"].as_s

          io = IO::Memory.new(content)

          WebMock.stub(:get, Regex.new(url)).to_return(status: 200, body_io: io)
        elsif mock.as_h["type"] == "file"
          name = mock.as_h["name"].as_s
          content = mock.as_h["content"]?.try(&.as_s) || "Content"

          mkdir_p(task.source_dir)
          File.write("#{task.source_dir}/#{name}", content)
        end
      end
    end

    def update_commands(defn, task)
      if src = task.source
        task.source(src.uri.to_s, extension: src.extension)
      end

      defn.try do |defn|
        unless defn["allow_patch"]? && (defn["allow_patch"]?.try(&.as_s) == "true")
          task.commands.reject! do |command|
            command.is_a?(Barista::Behaviors::Software::Commands::Patch)  
          end
        end
      end
    end

    # installers are things that get installed.
    def prepare_installs(defn, task)
      mkdir_p(task.smart_install_dir)

      defn["binaries"]?.try do |binaries|
        binaries.as_a.each do |name|
          path = Path.new("#{task.smart_install_dir}/embedded/bin/#{name}")
          mkdir_p(path.dirname)
          File.write(path.to_s, binary_data)
        end
      end

      defn["libaries"]?.try do |libs|
        mkdir_p("#{task.smart_install_dir}/embedded/lib")

        libs.as_a.each do |name|
          File.write("#{task.smart_install_dir}/embedded/lib/#{name}", binary_data)
        end
      end
    end

    def mock_task_file(task)
      task.source.try do |src|
        if Dir.exists?(task_path(task))
          gzip_task_path(task)

          WebMock.stub(:get, src.uri.to_s).to_return do |request|
            headers = HTTP::Headers.new.merge!({ "Content-Type" => "application/gzip", "Content-Encoding" => "gzip" })
            HTTP::Client::Response.new(200, body_io: File.open("#{task_path(task)}.tar.gz"), headers: headers)
          end
        end
      end
    end

    private def gzip_task_path(task)
      Process.run("tar -czvf #{task.name}.tar.gz #{task.name}", shell: true, env: env, chdir: path)
    end

    def file_path(task, name)
      p = Path.new(task_path(task), name)
      mkdir_p(p.dirname) unless Dir.exists?(p.dirname)
      p.to_s
    end

    def task_path(task)
      File.join(path, task.name)
    end

    def reset_env
      ENV.keys.each do |key|
        env[key] = ENV[key]
        ENV.delete(key)
      end

      ENV["PATH"] = "#{path}/system"

      env
    end

    def restore_env
      @env.try do |env|
        env.keys.each do |key|
          ENV[key] = env[key]
        end
      end
    end

    def mock_system_binary(binary)
      File.write("#{system_path}/#{binary}", binary_data)
    end

    def system_path
      p = File.join(path, "system")
      mkdir_p(p)
      p
    end
    
    def register_needed_binaries
      ["tar", "cp", "sh", "basename", "gzip"].each do |bin|
        register_symlink(bin)
      end
    end
  end
end