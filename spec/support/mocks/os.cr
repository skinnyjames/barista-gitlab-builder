record TestMemory, cpus : Int32 = 4
record TestKernel, machine : String = "x86_64"
record TestPlatform, name : String = "ubuntu", family : String = "debian", version : String = "1.2.3"

module Barista::Behaviors::Software::OS::Information
  def memory
    TestMemory.new
  end

  def kernel
    TestKernel.new
  end

  def platform
    TestPlatform.new
  end
end