require "../../spec_helper"

module Gitlab
  class MockVersion
    include VersionFinder
    
    getter(
      :ee,
      :commit_sha,
      :semver_version,
      :gitlab_host,
      :gitlab_project,
      :gitlab_version,
      :gitaly_version,
      :gitlab_internal_version,
      :gitlab_pages_version,
      :gitlab_shell_version,
      :gitlab_workhorse_version,
      :gitlab_metrics_exporter_version,
      :gitlab_kas_version,
      :gitlab_elasticsearch_indexer_version
    )

    @gitlab_host = "foo.bar"
    @gitlab_project = "gitlab"
    @retry = 3
    @ee : Bool = true
    @commit_sha : String = "foobar"
    @gitlab_internal_version : String = "1.2.3"
    @semver_version : String = "1.2.3"
    @gitlab_version : String = "1.2.3"
    @gitaly_version : String = "1.2.3"
    @gitlab_pages_version : String = "1.2.3"
    @gitlab_shell_version : String = "1.2.3"
    @gitlab_workhorse_version : String = "1.2.3"
    @gitlab_metrics_exporter_version : String = "1.2.3"
    @gitlab_kas_version : String = "1.2.3"
    @gitlab_elasticsearch_indexer_version : String = "1.2.3"

    def initialize(version, ee : Bool); end

    def ee?
      @ee
    end
  end
end