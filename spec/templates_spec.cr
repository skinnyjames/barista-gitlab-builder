private def get_package_script_path(script)
  "#{project.install_dir}/.package_util/package-scripts/#{script}"
end

private def get_cookbook_path(script)
  "#{project.install_dir}/embedded/cookbooks/#{script}"
end

private def get_rails_path(script)
  "#{project.install_dir}/bin/#{script}"
end

private def get_runit_path(script)
  "#{project.install_dir}/embedded/bin/#{script}"
end

private def external_url_script
  task("package-scripts").file("external_url")
end

describe "Templates" do
  describe "package-scripts" do
    it "scripts exist" do
      ["postinst", "postrm", "posttrans", "preinst"].each do |file|
        file_should_exist(get_package_script_path(file))
      end
    end

    it "postinst has DEST_DIR set" do
      path = get_package_script_path("postinst")

      file_should_contain(path, "DEST_DIR=#{project.install_dir}")
    end

    it "preinst has DEST_DIR set" do
      path = get_package_script_path("preinst")

      expected = [
        "DEST_DIR=#{project.install_dir}",
        "NEW_MAJOR_VERSION=#{build_version_major}",
        "NEW_MINOR_VERSION=#{build_version_minor}"
      ]

      file_should_contain(path, expected)    
    end

    it "postinst has DEST_DIR and a script to resolve external urls" do 
      path = get_package_script_path("postinst")

      file_should_contain(path, ["DEST_DIR=#{project.install_dir}", external_url_script])
    end
  end
  
  describe "gitlab-cookbooks" do
    it "provides json files" do
      %w[dna geo-postgresql-config patroni-config pg-upgrade-config postgresql-bin postgresql-config].each do |script|
        path = get_cookbook_path("#{script}.json")
        file_should_contain(path, "gitlab-ee")
      end
    end
  end

  describe "gitlab-rails" do
    it "ships wrappers for common ruby commands" do
      %w[gitlab-rails gitlab-rake gitlab-backup].each do |script|
        path = get_rails_path("#{script}")
        file_should_contain(path, project.install_dir)
      end
    end
  end

  describe "gitaly" do
    puts "TODO: revisist"
  end

  describe "runit" do
    it "provides a wrapper for runsvdir" do
      expected = "PATH=#{project.install_dir}/bin:#{project.install_dir}"
      file_should_contain(get_runit_path("runsvdir-start"), expected)
    end
  end
end