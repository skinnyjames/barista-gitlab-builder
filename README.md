# barista_gitlab_builder

Concurrently builds a package for GitLab Community | Enterprise Edition using Barista.

Currently tested with GitLab Community Edition using AMD64

Please see the [documentation](https://gitlab-org.gitlab.io/quality/barista-gitlab-builder) for more information
