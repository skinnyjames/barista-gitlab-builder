# Getting Started

## Project overview

The Barista GitLab Builder project is made up of a few folders which are used during the build lifecycle.

For instructions on running a build, please see the [Build Instructions](#build-instructions)

### src/

It contains software defintions the implement `Barista::Behaviors::Omnibus` behaviors, as well as any 
extra files that are packaged with the build.

* **files/** contains the files that are packaged with the build.  It includes cookbooks, gitlab-ctl commands, and package scripts.
* **patches/** contains any software patches that are applied to downloaded source code
* **templates/** contains [Crinja](https://github.com/straight-shoota/crinja) templates that are used to generate files that are packaged with the build
* **gitlab/** contains project defintions and helpers that are provided to the projects and tasks
    * **cli_commands/** contains classes that implement `ACON::Command` and are added to the binary
    * **projects/** contains the gitlab project definitions
    * **tasks/** contains the software build defintions

### docker/

This is where the build image templates are stored.  These templates generate a docker image containing the needed 
dependencies to perform a build.  This project uses and bundles [TekWizely/bash-tpl](https://github.com/TekWizely/bash-tpl) to handle the Dockerfile templates.  Please see that project for examples of how it works.

!!! note

    Dockerfile templates are stored in this directory with the name `Dockerfile_<platform>_<version>(_<arch>).tpl`

    For Ubuntu 22.04, the filename is `Dockerfile_ubuntu_22.04.tpl`

* **scripts/** contains any scripts to aid in generating the buildable Dockerfile.
* **snippets/** contains any docker snippets to included in the Docker template.

### package/

This folder contains the `Dockerfile` and assets used by `Gitlab::Tasks::PackageAndPush` to produce and publish a distributable 
docker image.  This image has that has the Debian package produced by `gitlab-builder` installed.  In other words, this folder is used by the build to make the testable image.

## Build Instructions

### Build a local package of GitLab

#### Prerequisites

* docker (for building)

This project is written in [Crystal](https://crystal-lang.org/) but does not require crystal in order to create the package, which can be done via a `Docker` container.

There is little reason to build this project locally since it 

* has build dependencies
* mutates the filesystem at `/opt`

### Creating the docker image

#### from the project

```
git clone https://gitlab.com/gitlab-org/quality/barista-gitlab-builder && cd barista-gitlab-builder
```

Since the builder creates files in /opt, we want to sandbox the build in a docker container 
with the needed tools to run the build.

We use a shell template script that references the suffixes 
of the Dockerfile templates in `./docker`

```
# makes a local image named gitlab_builder:ubuntu_22.04
./build_image ubuntu_22.04
```

If building for ARM architectures
```
./build_image ubuntu_22.04_arm
```

#### from the registry
```
docker pull registry.gitlab.com/gitlab-org/quality/barista-gitlab-builder:ubuntu_22.04
```
### Running the build 

When building locally the task runner expects for a volume mount to be located at `/cache`

This is used for caching build artifacts, and will speed up subsequent builds.

Make a volume mount (if it doesn't exist) and run the new image

**Note** you can provide a port that targets container port `80` if you want to verify the build in a browser.

```
mkdir ./volume
docker run -v `pwd`/volume:/cache -it -p 3001:80 gitlab_builder:ubuntu_22.04 bash
```

Inside the docker container we can use the `gitlab-builder` cli.
```
# help information
./gitlab-builder build --help
```

The usage for the cli is

```
gitlab-builder build --ref=<branch|tag|commit>(default: master) --workers=(default: # of cpus) <gitlab-ce|gitlab-ee|gitlab-fips>(default: gitlab-ce)
```

To build gitlab-ee (master) using 7 concurrent workers
```
./gitlab-builder build --workers=7 gitlab-ee
```

This will produce an OS package in
`/opt/barista/package`

If you want to save this package, you can move it to `/cache`

**Note** you can also build from branches, tags, or commits by passing the `--ref` flag.


### Verifying the build

The chef cookbooks used to configure gitlab don't work directly in a Docker container.

We can emulate an OS environment by installing `systemctl`

```
apt install -y systemctl
mkdir /run/systemd/system

cd /opt/gitlab/bin && ./gitlab-ctl reconfigure
```

This should correctly reconfigure GitLab and provide access to the instance at `http://localhost:3001`

!!! note
    
    The root credential can be found at `/etc/gitlab/initial_root_password`
