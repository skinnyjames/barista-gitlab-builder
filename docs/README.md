# Barista GitLab Builder

Barista GitLab Builder is a CLI to build and package GitLab (Community or Enterprise) edition.

It is written in Crystal and uses the [Barista](https://skinnyjames.gitlab.io/barista) library to orchestrate a concurrent build.

!!! note 

    Barista itself is a task runner framework that ships with a set of behaviors.

    Barista GitLab Builder is a set of build definitions for building GitLab specifically.

    For more questions and concerns regarding the mechanics of the build, please see the docs for [Barista::Behaviors::Omnibus](https://skinnyjames.gitlab.io/barista/behaviors/omnibus/)

## Goals for this project

The goal of this project is to produce a GitLab edition at any commit quickly and accurately.  It uses a concurrent build process and cache to accomplish this.

The impetus for this project is to improve the build speed of GitLab editions for [testing purposes](https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/1524).  This is not to be confused with [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab) which also produces GitLab editions for distribution purposes.

### Why Crystal?

This decision was made from a few factors.

* Crystal has [concurrency mechanisms](https://crystal-lang.org/reference/1.7/guides/concurrency.html) baked in using fibers and channels
* Crystal has an API that is very similar to ([and sometimes interchangeable with](https://crystal-lang.org/reference/1.6/crystal_for_rubyists/index.html#using-the-crystal-command)) Ruby (with some critical differences)
* Crystal provides [type safety](https://crystal-lang.org/reference/1.6/syntax_and_semantics/type_restrictions.html)

!!! note

    Type safety can drastically reduce the feedback loop when developing against a long running process.
    
    Incorrect typings or typos will fail during compile instead of raising a Runtime error deep into the build.

### Why a new project?

[Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab) is written in Ruby using [Chef Omnibus](https://github.com/chef/omnibus).  Chef Omnibus is a sophisticated DSL to producing full stack installers, but has some limitations:

`The build is created serially`

:   Since Omnibus produces a serial/sequential build, unrelated software definitions end up blocking each other when they don't need to.

    Barista GitLab Builder captializes on available cpus by building unrelated software defintions concurrently.

`The cache is incremental`

:   Omnibus employs an [incremental git cache](https://github.com/chef/omnibus/blob/main/docs/Build%20Cache.md) designed to reduce build times.  This becomes problematic when rebuilds on a changed defintion triggers rebuilds on unrelated software that come "after" it.

    Barista GitLab Builder employs a concurrent cache with isolated artifacts.
    The filename of the artifact contains the digest of the task and its upstream dependencies, as well as the platform and architecture.

`It isn't easily extensible`

:   Omnibus is written as a deep DSL to produce build images.  While providing an interface for the build, one must monkey patch it to change the behavior.

    Barista is written as a light interface with behaviors exposed as mixins.  Since everything is Barista GitLab Builder is a plain class, it can mixin any custom functionality and inject tasks with custom runtime state.

### Benchmarks

On average and depending on the build cache, Omnibus GitLab builds can take [50](https://gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/-/pipelines/660357032) to [90](https://gitlab.com/gitlab-org/gitlab/-/jobs/2951784928) minutes to build on a 32 cpu machine.

On the same machine, Barista GitLab Builder can do approximately the same work in [23](https://gitlab.com/gitlab-org/quality/barista-gitlab-builder/-/jobs/3576907250) minutes

!!! note

    This is a best effort apples to apples comparison as opposed to an exact comparison.

    Barista GitLab Builder also produces a GitLab QA image and bottlenecks on work that cannot be done concurrently, such as producing the final build image.

    The biggest gains in speed happen earlier in the build when there is a large amount of unrelated work that can be executed concurrently.


If on average, Barista GitLab Builder saves ~25 minutes then we can estimate yearly hours saved as such:

where `x` is the number of builds run in a month:

```
((x * 25) * 12) / 60)
```

Building *1500* to *2000* times a month (such as in [package-and-test](https://dashboards.quality.gitlab.net/d/2s9gp4nVk/test-metrics?orgId=1&from=now-1M%2FM&to=now-1M%2FM&viewPanel=10)) would yield around *7.5k* to *10k* saved pipeline hours a year.







