# Command line interface

Barista GitLab Builder exposes a command line interface to run commands against this project.

These commands are located in `src/gitlab/cli_commands` and are imported to `src/gitlab/gitlab_builder.cr`

The most important one is `build_project`, which compiles the software dependencies and produces the platform package.

However, more commands can easily be added, such as producing an artifact or output based on the dependency graph.  Please see the [Barista docs](https://skinnyjames.gitlab.io/barista/cli/) for more information.