# Testing

The specs for Barista GitLab Builder includes helper tooling for stubbing the build environment and shell commands.

This allows the entire project to be "built" in a fast and safe way.

`Gitlab::GitLabEE` is built before the suite runs, and the tests can assert on the build artifacts.

!!! note

    When running the specs, all the variables are removed from `ENV` and replaced with paths to files that are specific to the spec.
    
    This is to prevent running unexpected or destructive commands on a local machine.

This tooling can be found in `spec/support`

## Support::Provider

The `Support::Provider` class is designed to process a YAML file that declares the build and task dependencies.  It can then hook into orchestration events to provide psuedo binaries and files as the task processes them.

For example:

```yaml
system:
  binaries:
    - make
tasks:
  task-installs-binary:
    provides:
      binaries:
        - configure
      license_files:
        - COPYING
    installs:
      binaries:
        - task-installs-binary
  task-provides-license:
    provides:
      license_files:
        - LICENSE
```
## system

The `system["binaries"]` takes an array of shell commands to be exposed to all of the tasks.

## tasks

The `tasks` key takes a hash of key / value pairs where

* `key` is the name of the task to target, and
* `value` is a hash with the following options.

### provides

The `provides` key signifies items that will be provided to the downloaded task source code, either directly or indirectly.

It works by intercepting a task's network request, and sending back a gzipped tarball that contains files that the task expects to operate on.

Consider the following task

```crystal
@[Barista::BelongsTo(Internal::Provides)]
class TaskInstallsBinary < Barista::Task
  include Barista::Behaviors::Omnibus::Task

  @@name = "task-installs-binary"

  def build : Nil
    env = with_embedded_path(with_standard_compiler_flags(with_destdir))

    command("./configure", env: env)
    command("make foo bar", env: env)
  end

  def configure : Nil
    version("1.0.0")
    license("MIT")
    license_file("COPYING")
    source("http://www.example.com/install-binary.tar.gz")
  end
end
```

This task reaches out to `example.com`, downloads and unpacks it's source code, and runs operations on it.

It is expecting a few things:

* A license file in the source directory named `COPYING`
* A binary in the source directory named `configure`
* A system-wide binary named `make`

Here is a corresponding YAML stub:

```yaml
system:
  binaries:
    - make
tasks:
  task-installs-binary:
    provides:
      - configure
    license_fiels:
      - COPYING

```

This will 

* create a new source code directory in the path configured for Support::Provider
* write a bash file named `configure` that echos any arguments that it receives.
* write a file named `COPYING`
* mock `http://www.example.com/install-binary.tar.gz` and send back a compressed version of the new source code directory

!!! warning

    While most commands are stubbed, some are needed for barista to function.

    These commands are generated on the spec run by symlinking host binaries.

    * a symlink to `basename`
    * a symlink to `cp`
    * a symlink to `sh`
    * a symlink to `tar`

!!! note

    Commands that use the Crystal API to perform work continue to operate normally

    Examples include

    * template
    * sync
    * copy
    * emit
    * link


#### binaries

Binaries can be added by their name by using the `binaries` key.

#### license_files

License files can be added by their name by using the `license_files` key.

#### files

Files can be added to the expected task source code by using the `files` key.

Example:
```yaml
tasks:
  a_task:
    provides:
      files:
        - name: hello.json
          content: "{ \"hello\": \"world\" }"
```

### mocks

Additional network and file mocks can be registered by using the `mocks` key.  

Mocks with a type of `api` will create a stub on the network request by performing a regex  on the `url` 

!!! info

    Sometimes a task adds a software patch to add files to an empty directory.  If the task doesn't download source code, `provides` won't work.
    In these cases we can mock the file directly.


Example
```yaml
tasks:
  cacerts:
    mocks:
      - url: curl.haxx.se
        content: "SOMECACERT"
        type: api
      - type: file
        name: "LICENSE"
        content: "Some license"
```

### installs

The `installs` key signfies files that a task should install to the `install_dir`

This is mostly helpful to test tasks that depend on the artifacts from another task. 

Example:

```yaml
tasks:
  ruby:
    installs:
      # binaries are stubbed at "#{project.install_dir}/embedded/bin"
      # libraries are stubbed at "#{project.install_dir}/embedded/lib"
      binaries:
        - ruby
        - gem
        - bundle
      libraries:
        - libffi.so
  chef-gem:
  # will call the `gem` command which is installed by ruby
  # ...
```
## Why?

The benefits of this approach are

* the whole flow between components can be tested
* any artifacts the build produces can be asserted against
* basic stubs can be replaced with more complex stubs to simulate and assert against more nuanced behaviors