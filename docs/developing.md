# Developing

As mentioned in the [Introduction](/quality/barista-gitlab-builder), this project is made with the Crystal language and the Barista framework.

As such, it is good to have a working understanding knowledge of [Crystal](https://crystal-lang.org/reference/1.6/syntax_and_semantics/index.html) and [Barista](https://skinnyjames.gitlab.io/barista/)

This project also uses `Python3` for generating documentation.

## Development dependencies

* Crystal [[Install docs]](https://crystal-lang.org/install/)
* Python [[asdf-python]](https://github.com/asdf-community/asdf-python)

## Setup project

```
git clone https://gitlab.com/gitlab-org/quality/barista-gitlab-builder.git && cd barista-gitlab-builder
shards install
crystal spec
```

### Building the project locally inside of a build image

See [Local build instructions](/quality/barista-gitlab-builder/getting_started/#build-a-local-package-of-gitlab)


### Generating docs

This project uses [mkdocs](https://squidfunk.github.io/mkdocs-material/) to generate the static documentation

The project comes with a make script to provision a [Python virtual environment](https://docs.python.org/3/library/venv.html), which is used to install dependencies and generate the documetnation.

To serve and watch for doc changes
* make serve

To build the docs
* make build


